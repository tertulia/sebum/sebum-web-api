<div align="center">

# *Sebum* 

# Conceived by  [Vanessa Mota](https://gitlab.com/vanessaoliveira2706)

## Developed by  [Vanessa Mota](https://gitlab.com/vanessaoliveira2706) & [Fernando de Oliveira](https://gitlab.com/FernandoDeOliveira)

<a href="https://www.paypal.com/donate?hosted_button_id=82Z99Y5ZNZNT2&source=url">
  <img src="https://www.paypalobjects.com/pt_BR/BR/i/btn/btn_donateCC_LG.gif" alt="Donate with PayPal" />
</a>

</div>

## Overview
- An application to facilitate and encourage the sharing of books among users freely, as well as to encourage the creation of reading clubs.
- An *Peer-to-Peer* application for books

## Why the *Sebum*?
- Avoid personal library idleness
- Know which people have the desired books
- Connect people with literary affinities


## Install dependencies
### Virtual environment
after clone this repo, in the root of the project, create a venv and install the dependencies
```
$ pip install virtualenv
$ virtualenv venv - python
$ source venv/bin/activate
$ pip install -r environments/requirements-dev.txt
```

    
