from django.db.models import Q
from django.shortcuts import render, redirect

from apps.bookshelves.models import BookShelf, SharedBookShelves
from apps.bookssimilarities.models import BooksSimilarities


def index(request):
    if request.user.is_authenticated:
        return redirect('home')
    return render(request, 'pages/index.html')


def home(request):
    if request.user.is_authenticated:
        bookshelves = BookShelf.objects.filter(owner=request.user)

        pk_books = bookshelves.values_list('pk', flat=True)
        books_similarities = BooksSimilarities.objects.filter(
            Q(book__pk__in=pk_books) | Q(similar__pk__in=pk_books)
        ).exclude(
            Q(book__pk__in=pk_books) & Q(similar__pk__in=pk_books)
        )

        shared_bookshelves = SharedBookShelves.objects.filter(
            Q(solicitor=request.user) | Q(bookshelf__owner=request.user)
        )

        context = dict(books_similarities=books_similarities,
                       bookshelves=bookshelves,
                       shared_bookshelves=shared_bookshelves)

        return render(request, 'pages/home.html', context=context)
    return redirect('index')
