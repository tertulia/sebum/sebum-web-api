from django.contrib.auth import get_user_model
from django.test import TestCase, Client
from django.urls import reverse

from apps.books.models import Book
from apps.bookshelves.models import BookShelf


class TestIndexViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.index_url = reverse('index')

    def test_index_template(self):
        response = self.client.get(self.index_url)
        self.assertTemplateUsed(response, 'pages/index.html')


class TestHomeView(TestCase):
    def setUp(self) -> None:
        self.client = Client()
        self.home_url = reverse('home')
        self.user = get_user_model().objects.create_user(email='user@email.com', password='some pass', name='user name')
        self.book = Book.objects.create(title='Catena aurea')
        self.bookshelf = BookShelf.objects.create(owner=self.user, book=self.book)

    def test_home_template_unauthenticated_user(self):
        response = self.client.get(self.home_url)
        self.assertRedirects(response, '/')

    def test_home_template_authenticated_user(self):
        self.client.force_login(self.user)
        response = self.client.get(self.home_url)
        self.assertTemplateUsed(response, "pages/home.html")
        self.assertTemplateUsed(response, "bookshelves/partials/bookshelf_widget.html")

    def test_user_book_shelves_is_in_context_home_page(self):
        self.client.force_login(self.user)
        response = self.client.get(self.home_url)
        self.assertIn(self.bookshelf, response.context['bookshelves'])
