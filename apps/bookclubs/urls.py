from django.urls import path

from . import views

urlpatterns = [
    path('', views.BookClubListView.as_view(), name='book-clubs'),
    path('<int:pk>/edit', views.EditBookClubView.as_view(), name='edit-book-club'),
]
