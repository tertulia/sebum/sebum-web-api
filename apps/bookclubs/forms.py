from django import forms

from apps.bookclubs.models import BookClub


class BookClubForm(forms.ModelForm):
    class Meta:
        model = BookClub
        exclude = ['admin']
        labels = dict(name='Nome',
                      description='Descrição',
                      admin='Administrador',
                      theme='Tema'
                      )
