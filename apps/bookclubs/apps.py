from django.apps import AppConfig


class BookclubsConfig(AppConfig):
    name = 'bookclubs'
