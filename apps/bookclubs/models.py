from django.db import models

from sebum_web_api import settings


class BookClub(models.Model):
    name = models.CharField(max_length=100)
    telegram = models.CharField(max_length=50)
    description = models.CharField(max_length=100)
    admin = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    theme = models.CharField(max_length=100)

    def __str__(self):
        return self.name
