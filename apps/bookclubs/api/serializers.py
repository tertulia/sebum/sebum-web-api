from rest_framework import serializers

from ..models import BookClub


class BookClubSerializer(serializers.ModelSerializer):
    admin_id = serializers.SerializerMethodField()
    admin_name = serializers.SerializerMethodField()
    admin_telephone = serializers.SerializerMethodField()

    class Meta:
        model = BookClub
        fields = ['name', 'telegram', 'description', 'admin_id', 'admin_name', 'admin_telephone', 'theme']

    def get_admin_id(self, book_club):
        return book_club.admin.id

    def get_admin_name(self, book_club):
        return book_club.admin.name

    def get_admin_telephone(self, book_club):
        return book_club.admin.telephone
