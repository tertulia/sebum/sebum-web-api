from rest_framework import filters, mixins, viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from .permissions import UpdateOwnBookClubs
from .serializers import BookClubSerializer
from ..models import BookClub


class BookClubViewSet(viewsets.GenericViewSet, mixins.CreateModelMixin, mixins.ListModelMixin,
                      mixins.UpdateModelMixin, mixins.RetrieveModelMixin):
    authentication_classes = (TokenAuthentication,)
    serializer_class = BookClubSerializer
    queryset = BookClub.objects.all()
    permission_classes = (UpdateOwnBookClubs, IsAuthenticated,)
    filter_backends = (filters.SearchFilter,)
    search_fields = ("name", "theme",)

    def perform_create(self, serializer):
        serializer.save(admin=self.request.user)
