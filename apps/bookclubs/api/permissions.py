from rest_framework import permissions


class UpdateOwnBookClubs(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return obj.admin.id == request.user.id
