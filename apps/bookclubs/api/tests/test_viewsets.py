from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from apps.bookclubs.api.serializers import BookClubSerializer
from apps.bookclubs.models import BookClub

BOOKCLUBS_URL = reverse('api:bookclubs-list')


def get_url(view_name, action, _id=None):
    if not _id:
        return reverse(f'api:{view_name}-{action}')
    return reverse(f'api:{view_name}-{action}', args=[_id])


def sample_book_club(admin, name="book club", telegram="@telegram",
                     description="a club about books", theme="read is important"):
    return BookClub.objects.create(name=name, telegram=telegram, description=description,
                                   admin=admin, theme=theme)


class TestEndpointBookClubs(TestCase):
    def setUp(self) -> None:
        self.data = dict(email="user@email.com", password="this pass", name="user name")
        self.user = get_user_model().objects.create(**self.data)
        self.client = APIClient()
        self.client.force_authenticate(self.user)
        self.book_club_data = dict(name="this book club", telegram="book club telegram",
                                   description="a nice description",
                                   theme="a cool theme")

    def test_create_book_club_success(self):
        response = self.client.post(BOOKCLUBS_URL, data=self.book_club_data)
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)

    def test_create_book_club_no_name(self):
        self.book_club_data.pop("name")
        response = self.client.post(BOOKCLUBS_URL, data=self.book_club_data)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)

    def test_create_book_club_no_telegram(self):
        self.book_club_data.pop("telegram")
        response = self.client.post(BOOKCLUBS_URL, data=self.book_club_data)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)

    def test_create_book_club_no_description(self):
        self.book_club_data.pop('description')
        response = self.client.post(BOOKCLUBS_URL, data=self.book_club_data)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)

    def test_create_book_club_no_theme(self):
        self.book_club_data.pop('theme')
        response = self.client.post(BOOKCLUBS_URL, data=self.book_club_data)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)

    def test_create_book_club_no_authentication(self):
        new_client = APIClient()
        response = new_client.post(BOOKCLUBS_URL, data=self.book_club_data)
        self.assertEqual(status.HTTP_401_UNAUTHORIZED, response.status_code)

    def test_get_all_bookclubs(self):
        sample_book_club(admin=self.user)
        sample_book_club(admin=self.user)
        sample_book_club(admin=self.user)

        new_client = APIClient()
        user = get_user_model().objects.create_user(email='newuser@email.com', password='this pass', name='user name')
        new_client.force_authenticate(user)

        serializer = BookClubSerializer(BookClub.objects.all(), many=True)

        response = new_client.get(BOOKCLUBS_URL)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)

    def test_update_book_club_by_admin(self):
        book_club = sample_book_club(admin=self.user)
        payload = dict(name='new name', telegram='@new telegram', description="new description", theme='other theme')
        url = get_url('bookclubs', 'detail', book_club.id)

        response = self.client.patch(url, payload)

        book_club.refresh_from_db()
        serializer = BookClubSerializer(book_club)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)

    def test_retrieve_book_club(self):
        book_club = sample_book_club(self.user)
        serializer = BookClubSerializer(book_club)
        url = get_url('bookclubs', 'detail', book_club.id)

        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(serializer.data, response.data)

    def test_update_book_club_by_not_admin(self):
        new_user = get_user_model().objects.create_user(email='new@email.com', name='new user', password='some pass')
        client = APIClient()
        client.force_authenticate(new_user)
        book_club = sample_book_club(self.user)
        payload = dict(name='new name', telegram='@new telegram', description="new description", theme='other theme')
        url = get_url('bookclubs', 'detail', book_club.id)
        response = client.patch(url, payload)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
