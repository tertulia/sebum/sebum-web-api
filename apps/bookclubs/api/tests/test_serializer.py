from django.test import TestCase

from apps.accounts.models import User
from apps.bookclubs.api.serializers import BookClubSerializer
from apps.bookclubs.models import BookClub


class TestBookClubSerializer(TestCase):
    def setUp(self) -> None:
        self.user = User.objects.create_user('user@email.com', 'username', 'pass')

        self.bookclub_attrs = dict(name='booksclub name', telegram="bookclub telegram",
                                   description='what is this about', admin=self.user,
                                   theme='theme os bookclub'
                                   )
        self.bookclub = BookClub.objects.create(**self.bookclub_attrs)
        self.bookclub_serializer = BookClubSerializer(instance=self.bookclub)

    def test_contains_expected_fields(self):
        books_club_fields = set(self.bookclub_serializer.data.keys())
        expected_fields = {'name', 'telegram', 'description', 'admin_id', 'admin_name', 'admin_telephone', 'theme'}

        self.assertEqual(books_club_fields, expected_fields, 'Fields must be the same')

    def test_fields_content(self):
        self.bookclub_attrs.pop('admin')
        self.bookclub_attrs['admin_id'] = self.user.id
        self.bookclub_attrs['admin_name'] = self.user.name
        self.bookclub_attrs['admin_telephone'] = self.user.telephone
        self.assertDictEqual(self.bookclub_serializer.data, self.bookclub_attrs)
