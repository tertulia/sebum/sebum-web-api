from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic import ListView, UpdateView

from apps.bookclubs.forms import BookClubForm
from apps.bookclubs.models import BookClub


class BookClubListView(LoginRequiredMixin, ListView):
    model = BookClub
    template_name = 'bookclubs/book_clubs.html'
    paginate_by = 15

    def get_queryset(self):
        return self.model.objects.filter(admin=self.request.user).order_by('name')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['book_form'] = BookClubForm()
        return context

    def post(self, request, *args, **kwargs):
        self.form = BookClubForm(self.request.POST or None)
        if self.form.is_valid():
            book_club = self.form.save(commit=False)
            book_club.admin = self.request.user
            book_club.save()
            messages.success(request, "Clube criado com sucesso")
            return self.get(request, *args, **kwargs)
        else:
            messages.warning(request, "Campo inválido! tente novamente")
            return self.get(request, *args, **kwargs)


class EditBookClubView(LoginRequiredMixin, UserPassesTestMixin, SuccessMessageMixin, UpdateView):
    permission_denied_message = 'Você não tem autorização para acessar essa página'
    model = BookClub
    form_class = BookClubForm
    template_name = 'bookclubs/edit_book_club.html'
    success_url = 'edit'
    success_message = "Clube atualizado com sucesso"

    def test_func(self):
        return self.request.user == self.get_object().admin
