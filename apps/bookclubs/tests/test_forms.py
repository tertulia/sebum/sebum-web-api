from django.contrib.auth import get_user_model
from django.test import TestCase

from ..forms import BookClubForm


class TestForms(TestCase):
    def setUp(self) -> None:
        self.user = get_user_model().objects.create_user(email='user@email.com',
                                                         name='user name',
                                                         password="some pass", )

    def test_book_club_form_valid_data(self):
        form = BookClubForm(data={'name': 'the name of the club',
                                  'telegram': 'the telegram club',
                                  'description': 'some nice description',
                                  'admin': self.user,
                                  'theme': 'a great theme'
                                  })
        self.assertTrue(form.is_valid())

    def test_book_club_form_invalid_data(self):
        form = BookClubForm(data={})
        self.assertFalse(form.is_valid())
