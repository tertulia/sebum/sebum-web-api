import http

from django.contrib.auth import get_user_model
from django.test import TestCase, Client
from django.urls import reverse

from apps.bookclubs.models import BookClub


class BaseConfigTest(TestCase):
    def setUp(self) -> None:
        self.client = Client()
        self.user = get_user_model().objects.create_user(email='user@email.com',
                                                         name='user name',
                                                         password="some pass", )
        self.other_user = get_user_model().objects.create_user(email='other@email.com',
                                                               name='user name',
                                                               password="some pass", )
        self.client.force_login(self.user)

    def sample_book_club(self, admin) -> BookClub:
        return BookClub.objects.create(name='a good club',
                                       telegram='some telegram',
                                       description='a nice description',
                                       admin=admin,
                                       theme='a great book club')


class TestBookClubListView(BaseConfigTest):
    url = reverse('book-clubs')

    def test_unauthenticated_client_redirect_to_login(self):
        unauthenticated_client = Client()
        response = unauthenticated_client.get(self.url)
        self.assertRedirects(response, '/accounts/login/?next=' + self.url)

    def test_book_clubs_templates_used(self):
        self.sample_book_club(admin=self.user)
        response = self.client.get(self.url)
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'bookclubs/book_clubs.html')
        self.assertTemplateUsed(response, 'bookclubs/partials/book_clubs_widget.html')

    def test_book_clubs_in_context(self):
        book_club = self.sample_book_club(self.user)
        response = self.client.get(self.url)
        self.assertIn(book_club, response.context['page_obj'])

    def test_only_admin_book_clubs_in_context(self):
        admin_book_club = self.sample_book_club(self.user)
        other_user_book_club = self.sample_book_club(self.other_user)
        response = self.client.get(self.url)
        self.assertIn(admin_book_club, response.context['page_obj'])
        self.assertNotIn(other_user_book_club, response.context['page_obj'])


class TestEditBookClubView(BaseConfigTest):
    def get_url(self, book_id):
        return reverse('edit-book-club', args=[book_id])

    def test_unauthenticated_client_redirect_to_login(self):
        unauthenticated_client = Client()
        book_club = self.sample_book_club(self.user)
        url = self.get_url(book_club.id)
        response = unauthenticated_client.get(url)
        self.assertRedirects(response, '/accounts/login/?next=' + url)

    def test_unauthorized_client_redirect_to_home(self):
        other_client = Client()
        other_client.force_login(self.other_user)
        book_club = self.sample_book_club(self.user)
        url = self.get_url(book_club.id)
        response = other_client.get(url)
        self.assertEqual(response.status_code, http.HTTPStatus.FORBIDDEN)

    def test_update_book_club(self):
        book_club = self.sample_book_club(self.user)
        data = dict(name='a new club name',
                    telegram='new telegram',
                    description='new description',
                    theme='new theme')

        response = self.client.post(self.get_url(book_club.id), data=data)
        self.assertEqual(response.status_code, http.HTTPStatus.FOUND)
        book_club.refresh_from_db()
        self.assertEqual(book_club.name, data['name'])
        self.assertEqual(book_club.telegram, data['telegram'])
        self.assertEqual(book_club.description, data['description'])
        self.assertEqual(book_club.theme, data['theme'])
