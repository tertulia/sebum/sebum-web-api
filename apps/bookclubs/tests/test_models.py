from django.contrib.auth import get_user_model
from django.test import TestCase

from ..models import BookClub


class TestBookClubModel(TestCase):
    def setUp(self) -> None:
        self.user = get_user_model().objects.create_user("user@email.com", "his name", "his pass")
        self.bookclub_attrs = dict(
            name="book club name",
            telegram="telegram of the book club",
            description="his club is about something",
            admin=self.user,
            theme="what is this club is about"
        )
        self.book_club = BookClub.objects.create(**self.bookclub_attrs)

    def test_str_representation_of_book_club(self):
        self.assertEqual(str(self.book_club), self.bookclub_attrs["name"], "__str__ of book club must be its name")

    def test_deletion_of_admin_on_cascade(self):
        self.user.delete()
        book_club = BookClub.objects.filter(id=1).first()
        self.assertIsNone(book_club, "deletion of user must delete book club as well")
