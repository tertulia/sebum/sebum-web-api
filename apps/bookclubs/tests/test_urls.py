from django.test import SimpleTestCase
from django.urls import reverse, resolve

from .. import views


class TestUrls(SimpleTestCase):
    def test_book_clubs(self):
        url = reverse('book-clubs')
        self.assertEqual(resolve(url).func.view_class, views.BookClubListView)

    def test_edit_book_club(self):
        url = reverse('edit-book-club', args=[1])
        self.assertEqual(resolve(url).func.view_class, views.EditBookClubView)
