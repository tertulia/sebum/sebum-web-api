from django.db import models

from apps.books.models import Book


class BooksSimilaritiesManager(models.Manager):
    def update_all_similarities(self):
        self.all()
        for book in Book.objects.all():
            for similarity, similar_book in book.get_similarities():
                books_similarities, is_created = BooksSimilarities.objects.get_or_create(book_id=book.id,
                                                                                         similar_id=similar_book.id)
                books_similarities.update_similarity(similarity)


class BooksSimilarities(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE, related_name="book_similar")
    similar = models.ForeignKey(Book, on_delete=models.CASCADE, related_name="similar_book")
    similarity = models.FloatField(null=True, blank=True)

    objects = BooksSimilaritiesManager()

    def __str__(self):
        return f"{self.book.title} is {self.similarity} to {self.similar.title}"

    def update_similarity(self, similarity):
        self.similarity = similarity
        self.save()
