from django.contrib import admin

from .models import BooksSimilarities

admin.site.register(BooksSimilarities)
