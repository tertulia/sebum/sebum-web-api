# Generated by Django 3.1.5 on 2021-01-23 16:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('books', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BooksSimilarities',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('similarity', models.FloatField(blank=True, null=True)),
                ('book', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='book_similar', to='books.book')),
                ('similar', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='similar_book', to='books.book')),
            ],
        ),
    ]
