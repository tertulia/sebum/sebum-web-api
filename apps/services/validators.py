from typing import List

from django.core.exceptions import ValidationError
from django.core.validators import BaseValidator
from django.utils.translation import gettext_lazy as _


class CPFValidator(BaseValidator):
    message = _('Enter a valid CPF.')
    code = 'invalid'
    divider = 11

    def __init__(self, limit_value=11):

        super().__init__(limit_value)

    def __call__(self, value: str):
        valid_cpf = self.validate_cpf(value)
        if not valid_cpf:
            raise ValidationError(self.message, self.code)

    def cpf_slicer(self, cpf: str) -> List[str]:
        single_digits, check_digits = cpf[:-2], cpf[-2:]
        return [single_digits, check_digits]

    def weighted_sum(self, number: str, first_weight: int) -> int:
        result = 0
        for weight, number in enumerate(number, start=first_weight):
            result += int(number) * weight
        return result

    def validate_cpf(self, cpf: str) -> bool:
        try:
            single_digits, check_digits = self.cpf_slicer(cpf)

            first_weight_sum = self.weighted_sum(single_digits, first_weight=1)
            first_verified_digit = self.verify_digit(first_weight_sum)

            second_weight_sum = self.weighted_sum(single_digits + str(first_verified_digit),
                                                  first_weight=0)
            second_verified_digit = self.verify_digit(second_weight_sum)

            return f'{first_verified_digit}{second_verified_digit}' == check_digits
        except ValueError:
            raise ValidationError(_('Enter the string only of numbers'), self.code)

    def verify_digit(self, digit: int) -> int:
        exception = 10
        result = digit % self.divider
        return 0 if result == exception else result
