from django.core.exceptions import ValidationError
from django.test import SimpleTestCase

from apps.services.validators import CPFValidator


class TestCPFValidator(SimpleTestCase):
    cpf_validator = CPFValidator(limit_value=11)
    single_digits, check_digits = '123456789', '09',
    cpf = single_digits + check_digits

    def test_cpf_slice(self):
        """Whether cpf is sliced return 3 parts: single digits, origin, check digits"""
        expected_digits = self.cpf_validator.cpf_slicer(self.single_digits + self.check_digits)
        self.assertEqual(expected_digits, [self.single_digits,
                                           self.check_digits])

    def test_weighted_sum(self):
        """Whether singles digits is weight summed return all numbers summed with is related weight"""
        expected_sum = 285  # 285 = 1*1 + 2*2 + ... + 8*8 + 9*9
        first_weight = 1
        weighted_sum = self.cpf_validator.weighted_sum(self.single_digits, first_weight)
        self.assertEqual(expected_sum, weighted_sum)

    def test_verifying_digit(self):
        """Whether weight sum is verified return modulus with divider number"""
        weight_sum = 281
        expected = weight_sum % self.cpf_validator.divider
        modulus = self.cpf_validator.verify_digit(weight_sum)
        self.assertEqual(expected, modulus)

    def test_verifying_digit_result_is_10(self):
        """Whether verified digit is 10 return 0"""
        weight_sum = 285
        expected = 0
        modulus = self.cpf_validator.verify_digit(weight_sum)
        self.assertEqual(expected, modulus)

    def test_validate_cpf(self):
        """Whether valid cpf its checked return True"""
        valid_cpf = self.cpf_validator.validate_cpf(self.cpf)
        self.assertTrue(valid_cpf)

    def test_validate_cpf_invalid_string(self):
        invalid_cpf = '1234567char'
        with self.assertRaises(ValidationError):
            self.cpf_validator.validate_cpf(invalid_cpf)

    def test_call_cpf_validator_invalid_cpf(self):
        invalid_cpf = '12345678912'
        with self.assertRaises(ValidationError):
            self.cpf_validator(invalid_cpf)
