from random import choices
from unittest import mock

from django.test import TestCase

from apps.accounts.models import User
from apps.books.models import Book, Author, Category
from apps.bookshelves.models import BookShelf
from .json_for_test import BOOKS_EVALUATIONS, EUCLIDEANS, A_VIDA_INTELECTUAL_SIMILARITIES

BOOK_DATA_CONTENT = {
    "items": [{
        "volumeInfo": {
            "title": "Claudii Galeni Opera Omnia",
            "authors": ["Karl Gottlob Kühn"],
            "industryIdentifiers": [
                {
                    "type": "ISBN_13",
                    "identifier": '9781108028486'
                },
                {
                    "type": "ISBN_10",
                    "identifier": '1108028489'
                }
            ],
            "categories": ["Medical"],
            "imageLinks": {
                "smallThumbnail": "http://books.google.com/books/content?id=raW-65SeIw0C&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api",
                "thumbnail": "http://books.google.com/books/content?id=raW-65SeIw0C&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api"
            },
        },
    }
    ]
}

BOOK_DICT = {
    "title": "Claudii Galeni Opera Omnia",
    "authors": ["Karl Gottlob Kühn"],
    'ISBN_13': '9781108028486',
    'ISBN_10': '1108028489',
    'image_url': "http://books.google.com/books/content?id=raW-65SeIw0C&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api",
    "categories": ["Medical"],
}


class TestBookMethods(TestCase):

    def create_user(self, name: str) -> User:
        return User.objects.create_user(name + "@email.com", name, "this pass")

    def setUp(self) -> None:

        users = []
        for book in BOOKS_EVALUATIONS:
            _book = Book.objects.create(title=book)

            for user_name in BOOKS_EVALUATIONS[book]:
                if user_name not in users:
                    users.append(user_name)
                    user = self.create_user(user_name)
                    evaluation = BOOKS_EVALUATIONS[book][user_name]
                    BookShelf.objects.create(owner=user, book=_book, evaluation=evaluation)

                else:
                    user = User.objects.get(email=user_name + "@email.com")
                    evaluation = BOOKS_EVALUATIONS[book][user_name]
                    BookShelf.objects.create(owner=user, book=_book, evaluation=evaluation)

    def test_euclidean(self):
        a_vida_intelectual = Book.objects.get(title="A Vida Intelectual".lower())
        imitacao_de_cristo = Book.objects.get(title="Imitação de Cristo".lower())

        coming = imitacao_de_cristo.euclidean_other_book(a_vida_intelectual)
        going = a_vida_intelectual.euclidean_other_book(imitacao_de_cristo)

        self.assertEqual(coming, going)

        a_educacao_da_vontade = Book.objects.get(title="Caminho de perfeição".lower())
        el_criterio = Book.objects.get(title="El Criterio".lower())
        not_in_common = a_educacao_da_vontade.euclidean_other_book(el_criterio)
        self.assertEqual(0, not_in_common)

        same_energy = a_educacao_da_vontade.euclidean_other_book(a_educacao_da_vontade)
        self.assertEqual(1, same_energy)

        any_one, other = choices(Book.objects.all(), k=2)
        self.assertTrue(any_one.euclidean_other_book(other) in EUCLIDEANS)

    def test_similarities(self):
        a_vida_intelectual = Book.objects.get(title="A Vida Intelectual".lower())
        a_vida_intelectual_similarities = a_vida_intelectual.get_similarities()

        for _test, original in zip(a_vida_intelectual_similarities, A_VIDA_INTELECTUAL_SIMILARITIES):
            self.assertEqual(_test[0], original[0])

    def test_similarities_with_num(self):
        a_vida_intelectual = Book.objects.get(title="A Vida Intelectual".lower())
        num_of_similars = 1
        a_vida_intelectual_similarities = a_vida_intelectual.get_similarities(num=num_of_similars)

        self.assertEqual(num_of_similars, len(a_vida_intelectual_similarities), 'num of similares must be 1')

    @mock.patch('apps.books.models.google_api.get_book_data',
                return_value=(None, False))
    def test_create_book_by_isbn_fail(self, get_book_data):
        isbn = '9781108028486'
        book = Book.objects.create_with_isbn(isbn)
        self.assertEqual(book, None,
                         'fail creation of book must be None')

    @mock.patch('apps.books.models.google_api.get_book_data',
                return_value=(BOOK_DICT, 200))
    def test_create_book_by_isbn_success(self, get_book_data):
        isbn = '9781108028486'
        book = Book.objects.create_with_isbn(isbn)
        self.assertNotEqual(book, None,
                            'creation of book must not be None')
        self.assertEqual(book.title, BOOK_DICT['title'].lower(), 'title must be equal')
        self.assertEqual(book.ISBN_13, BOOK_DICT['ISBN_13'], 'ISBN_13 must be equal')
        self.assertEqual(book.image_url, BOOK_DICT['image_url'], 'image_url must be equal')

    def test_title_save_low_case(self):
        title = 'A FUNNY HistoRy'
        book = Book.objects.create(title=title)

        self.assertEqual(book.title, title.lower())

    def test_get_all_authors_as_one_string(self):
        book = Book.objects.create(title='some novel')
        book.authors.add(Author.objects.create(name="Author 1"))
        book.authors.add(Author.objects.create(name="Author 2"))
        book.authors.add(Author.objects.create(name="Author 3"))

        authors_in_one_string = ', '.join(a.name for a in Author.objects.all())

        self.assertEqual(authors_in_one_string, book.get_all_authors_as_one_string())

    def test_get_all_categories_as_one_string(self):
        book = Book.objects.create(title='some novel')
        book.categories.add(Category.objects.create(name="Category 1"))
        book.categories.add(Category.objects.create(name="Category 2"))
        book.categories.add(Category.objects.create(name="Category 3"))

        categories_in_one_string = ', '.join(a.name for a in Category.objects.all())

        self.assertEqual(categories_in_one_string, book.get_all_categories_as_one_string())


class TestAuthors(TestCase):
    def test_create_success(self):
        author = Author.objects.create(name='Moises')
        self.assertTrue(Author.objects.filter(name=author.name).exists())

    def test_author_representation(self):
        author = Author.objects.create(name='Moises')
        self.assertEqual(str(author), author.name)


class TestCategories(TestCase):
    def test_create_success(self):
        category = Category.objects.create(name='Moises')
        self.assertTrue(Category.objects.filter(name=category.name).exists())

    def test_author_representation(self):
        category = Category.objects.create(name='Moises')
        self.assertEqual(str(category), category.name)
