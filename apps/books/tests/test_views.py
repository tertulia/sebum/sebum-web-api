from django.contrib.auth import get_user_model
from django.test import TestCase, Client
from django.urls import reverse


class TestSearchBook(TestCase):
    def setUp(self) -> None:
        self.client = Client()
        self.user = get_user_model().objects.create_user(email='user@email.com',
                                                         name='user name',
                                                         password="some pass", )
        self.other_user = get_user_model().objects.create_user(email='other@email.com',
                                                               name='user name',
                                                               password="some pass", )
        self.client.force_login(self.user)
        self.search_book_url = reverse('search_books')

    def test_unauthenticated_client_redirect_to_login(self):
        unauthenticated_client = Client()
        response = unauthenticated_client.get(self.search_book_url)
        self.assertRedirects(response, '/accounts/login/?next=' + self.search_book_url)

    def test_search_books_requested_templates_used(self):
        response = self.client.get(self.search_book_url)
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'books/search_books.html')
        self.assertTemplateUsed(response, 'books/partials/book_widget.html')

    def test_context_of_search_books_page(self):
        response = self.client.get(self.search_book_url)
        self.assertIsNotNone(response.context.get("books", None))
        self.assertIsNotNone(response.context.get("searched_book", None))
