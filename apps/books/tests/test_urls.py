from django.test import TestCase
from django.urls import reverse, resolve

from apps.books import views


class TestBooksUrls(TestCase):
    def test_book_search_url(self):
        url = reverse('search_books')
        self.assertEqual(resolve(url).func, views.search_book)
