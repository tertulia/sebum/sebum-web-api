from unittest import mock

from django.test import TestCase
from rest_framework import status

from apps.books.search_module import google_api as g_api

ISBN_13_OK = '9780802863867'
ISBN_13_BAD = 'xxxxxxxxxxxxxx'
ISBN_10_OK = '0802863868'
ISBN_10_BAD = 'xxxxxxxxxx'
BOOK_DATA_CONTENT = {
    "totalItems": 3,
    "items": [
        {
            "kind": "books#volume",
            "id": "run-CQAAQBAJ",
            "etag": "Qz7x0/XVlzo",
            "selfLink": "https://www.googleapis.com/books/v1/volumes/run-CQAAQBAJ",
            "volumeInfo": {
                "title": "Imitação de Cristo",
                "subtitle": "",
                "authors": [
                    "Tomás de Kempis"
                ],
                "publisher": "Sociedade das Ciências Antigas",
                "publishedDate": "1979",
                "description": "Da imitação de Cristo e desprezo de todas as vaidades do mundo 1. Quem me segue não anda nas trevas, diz o Senhor (Jo 8,12). São estas as palavras de Cristo, pelas quais somos advertidos que imitemos sua vida e seus costumes, se verdadeiramente queremos ser iluminados e livres de toda cegueira de coração. Seja, pois, o nosso principal empenho meditar sobre a vida de Jesus Cristo. 2. A doutrina de Cristo é mais excelente que a de todos os santos, e quem tiver seu espírito encontrará nela um maná escondido. Sucede, porém, que muitos, embora ouçam frequentemente o Evangelho, sentem nele pouco enlevo: é que não possuem o espírito de Cristo. Quem quiser compreender e saborear plenamente as palavras de Cristo, é-lhe preciso que procure conformar à dele toda a sua vida. 3. Que te aproveita discutires sabiamente sobre a SS. Trindade, se não és humilde, desagradando, assim, a essa mesma Trindade? Na verdade, não são palavras elevadas que fazem o homem justo; mas é a vida virtuosa que o torna agradável a Deus. Prefiro sentir a contrição dentro de minha alma, a saber defini-la. Se soubesses de cor toda a Bíblia e as sentenças de todos os filósofos, de que te serviria tudo isso sem a caridade e a graça de Deus? Vaidade das vaidades, e tudo é vaidade (Ecle 1,2), senão amar a Deus e só a ele servir. A suprema sabedoria é esta: pelo desprezo do mundo tender ao reino dos céus. 4. Vaidade é, pois, buscar riquezas perecedoras e confiar nelas. Vaidade é também ambicionar honras e desejar posição elevada. Vaidade, seguir os apetites da carne e desejar aquilo pelo que, depois, serás gravemente castigado. Vaidade, desejar longa vida e, entretanto, descuidar-se de que seja boa. Vaidade, só atender à vida presente sem providenciar para a futura. Vaidade, amar o que passa tão rapidamente, e não buscar, pressuroso, a felicidade que sempre dura. 5. Lembra-te a miúdo do provérbio: Os olhos não se fartam de ver, nem os ouvidos de ouvir (Ecle 1,8). Portanto, procura desapegar teu coração do amor às coisas visíveis e afeiçoá-lo às invisíveis: pois aqueles que satisfazem seus apetites sensuais mancham a consciência e perdem a graça de Deus.",
                "readingModes": {
                    "text": False,
                    "image": False
                },
                "pageCount": 92,
                "printType": "BOOK",
                "maturityRating": "NOT_MATURE",
                "allowAnonLogging": False,
                "contentVersion": "1.0.0.0.preview.0",
                "panelizationSummary": {
                    "containsEpubBubbles": False,
                    "containsImageBubbles": False
                },
                "imageLinks": {
                    "smallThumbnail": "http://books.google.com/books/content?id=run-CQAAQBAJ&printsec=frontcover&img=1&zoom=5&source=gbs_api",
                    "thumbnail": "http://books.google.com/books/content?id=run-CQAAQBAJ&printsec=frontcover&img=1&zoom=1&source=gbs_api"
                },
                "language": "pt",
                "previewLink": "http://books.google.com.br/books?id=run-CQAAQBAJ&dq=imita%C3%A7%C3%A3o+de+cristo&hl=&cd=1&source=gbs_api",
                "infoLink": "http://books.google.com.br/books?id=run-CQAAQBAJ&dq=imita%C3%A7%C3%A3o+de+cristo&hl=&source=gbs_api",
                "canonicalVolumeLink": "https://books.google.com/books/about/Imita%C3%A7%C3%A3o_de_Cristo.html?hl=&id=run-CQAAQBAJ"
            },
            "saleInfo": {
                "country": "BR",
                "saleability": "NOT_FOR_SALE",
                "isEbook": False
            },
            "accessInfo": {
                "country": "BR",
                "viewability": "NO_PAGES",
                "embeddable": False,
                "publicDomain": False,
                "textToSpeechPermission": "ALLOWED",
                "epub": {
                    "isAvailable": False
                },
                "pdf": {
                    "isAvailable": False
                },
                "webReaderLink": "http://play.google.com/books/reader?id=run-CQAAQBAJ&hl=&printsec=frontcover&source=gbs_api",
                "accessViewStatus": "NONE",
                "quoteSharingAllowed": False
            },
            "searchInfo": {
                "textSnippet": "Da imitação de Cristo e desprezo de todas as vaidades do mundo 1."
            }
        },
        {
            "kind": "books#volume",
            "id": "run-CQAAQBAJ",
            "etag": "Qz7x0/XVlzo",
            "selfLink": "https://www.googleapis.com/books/v1/volumes/run-CQAAQBAJ",
            "volumeInfo": {
                "title": "Imitação de Cristo",
                "subtitle": "",
                "authors": [
                    "Tomás de Kempis"
                ],
                "publisher": "Sociedade das Ciências Antigas",
                "publishedDate": "1979",
                "description": "Da imitação de Cristo e desprezo de todas as vaidades do mundo 1. Quem me segue não anda nas trevas, diz o Senhor (Jo 8,12). São estas as palavras de Cristo, pelas quais somos advertidos que imitemos sua vida e seus costumes, se verdadeiramente queremos ser iluminados e livres de toda cegueira de coração. Seja, pois, o nosso principal empenho meditar sobre a vida de Jesus Cristo. 2. A doutrina de Cristo é mais excelente que a de todos os santos, e quem tiver seu espírito encontrará nela um maná escondido. Sucede, porém, que muitos, embora ouçam frequentemente o Evangelho, sentem nele pouco enlevo: é que não possuem o espírito de Cristo. Quem quiser compreender e saborear plenamente as palavras de Cristo, é-lhe preciso que procure conformar à dele toda a sua vida. 3. Que te aproveita discutires sabiamente sobre a SS. Trindade, se não és humilde, desagradando, assim, a essa mesma Trindade? Na verdade, não são palavras elevadas que fazem o homem justo; mas é a vida virtuosa que o torna agradável a Deus. Prefiro sentir a contrição dentro de minha alma, a saber defini-la. Se soubesses de cor toda a Bíblia e as sentenças de todos os filósofos, de que te serviria tudo isso sem a caridade e a graça de Deus? Vaidade das vaidades, e tudo é vaidade (Ecle 1,2), senão amar a Deus e só a ele servir. A suprema sabedoria é esta: pelo desprezo do mundo tender ao reino dos céus. 4. Vaidade é, pois, buscar riquezas perecedoras e confiar nelas. Vaidade é também ambicionar honras e desejar posição elevada. Vaidade, seguir os apetites da carne e desejar aquilo pelo que, depois, serás gravemente castigado. Vaidade, desejar longa vida e, entretanto, descuidar-se de que seja boa. Vaidade, só atender à vida presente sem providenciar para a futura. Vaidade, amar o que passa tão rapidamente, e não buscar, pressuroso, a felicidade que sempre dura. 5. Lembra-te a miúdo do provérbio: Os olhos não se fartam de ver, nem os ouvidos de ouvir (Ecle 1,8). Portanto, procura desapegar teu coração do amor às coisas visíveis e afeiçoá-lo às invisíveis: pois aqueles que satisfazem seus apetites sensuais mancham a consciência e perdem a graça de Deus.",
                "readingModes": {
                    "text": False,
                    "image": False
                },
                "pageCount": 92,
                "printType": "BOOK",
                "maturityRating": "NOT_MATURE",
                "allowAnonLogging": False,
                "contentVersion": "1.0.0.0.preview.0",
                "panelizationSummary": {
                    "containsEpubBubbles": False,
                    "containsImageBubbles": False
                },
                "imageLinks": {
                    "smallThumbnail": "http://books.google.com/books/content?id=run-CQAAQBAJ&printsec=frontcover&img=1&zoom=5&source=gbs_api",
                    "thumbnail": "http://books.google.com/books/content?id=run-CQAAQBAJ&printsec=frontcover&img=1&zoom=1&source=gbs_api"
                },
                "language": "pt",
                "previewLink": "http://books.google.com.br/books?id=run-CQAAQBAJ&dq=imita%C3%A7%C3%A3o+de+cristo&hl=&cd=1&source=gbs_api",
                "infoLink": "http://books.google.com.br/books?id=run-CQAAQBAJ&dq=imita%C3%A7%C3%A3o+de+cristo&hl=&source=gbs_api",
                "canonicalVolumeLink": "https://books.google.com/books/about/Imita%C3%A7%C3%A3o_de_Cristo.html?hl=&id=run-CQAAQBAJ"
            },
            "saleInfo": {
                "country": "BR",
                "saleability": "NOT_FOR_SALE",
                "isEbook": False
            },
            "accessInfo": {
                "country": "BR",
                "viewability": "NO_PAGES",
                "embeddable": False,
                "publicDomain": False,
                "textToSpeechPermission": "ALLOWED",
                "epub": {
                    "isAvailable": False
                },
                "pdf": {
                    "isAvailable": False
                },
                "webReaderLink": "http://play.google.com/books/reader?id=run-CQAAQBAJ&hl=&printsec=frontcover&source=gbs_api",
                "accessViewStatus": "NONE",
                "quoteSharingAllowed": False
            },
            "searchInfo": {
                "textSnippet": "Da imitação de Cristo e desprezo de todas as vaidades do mundo 1."
            }
        },
        {
            "kind": "books#volume",
            "id": "run-CQAAQBAJ",
            "etag": "Qz7x0/XVlzo",
            "selfLink": "https://www.googleapis.com/books/v1/volumes/run-CQAAQBAJ",
            "volumeInfo": {
                "title": "Imitação de Cristo",
                "subtitle": "",
                "authors": [
                    "Tomás de Kempis"
                ],
                "publisher": "Sociedade das Ciências Antigas",
                "publishedDate": "1979",
                "description": "Da imitação de Cristo e desprezo de todas as vaidades do mundo 1. Quem me segue não anda nas trevas, diz o Senhor (Jo 8,12). São estas as palavras de Cristo, pelas quais somos advertidos que imitemos sua vida e seus costumes, se verdadeiramente queremos ser iluminados e livres de toda cegueira de coração. Seja, pois, o nosso principal empenho meditar sobre a vida de Jesus Cristo. 2. A doutrina de Cristo é mais excelente que a de todos os santos, e quem tiver seu espírito encontrará nela um maná escondido. Sucede, porém, que muitos, embora ouçam frequentemente o Evangelho, sentem nele pouco enlevo: é que não possuem o espírito de Cristo. Quem quiser compreender e saborear plenamente as palavras de Cristo, é-lhe preciso que procure conformar à dele toda a sua vida. 3. Que te aproveita discutires sabiamente sobre a SS. Trindade, se não és humilde, desagradando, assim, a essa mesma Trindade? Na verdade, não são palavras elevadas que fazem o homem justo; mas é a vida virtuosa que o torna agradável a Deus. Prefiro sentir a contrição dentro de minha alma, a saber defini-la. Se soubesses de cor toda a Bíblia e as sentenças de todos os filósofos, de que te serviria tudo isso sem a caridade e a graça de Deus? Vaidade das vaidades, e tudo é vaidade (Ecle 1,2), senão amar a Deus e só a ele servir. A suprema sabedoria é esta: pelo desprezo do mundo tender ao reino dos céus. 4. Vaidade é, pois, buscar riquezas perecedoras e confiar nelas. Vaidade é também ambicionar honras e desejar posição elevada. Vaidade, seguir os apetites da carne e desejar aquilo pelo que, depois, serás gravemente castigado. Vaidade, desejar longa vida e, entretanto, descuidar-se de que seja boa. Vaidade, só atender à vida presente sem providenciar para a futura. Vaidade, amar o que passa tão rapidamente, e não buscar, pressuroso, a felicidade que sempre dura. 5. Lembra-te a miúdo do provérbio: Os olhos não se fartam de ver, nem os ouvidos de ouvir (Ecle 1,8). Portanto, procura desapegar teu coração do amor às coisas visíveis e afeiçoá-lo às invisíveis: pois aqueles que satisfazem seus apetites sensuais mancham a consciência e perdem a graça de Deus.",
                "readingModes": {
                    "text": False,
                    "image": False
                },
                "pageCount": 92,
                "printType": "BOOK",
                "maturityRating": "NOT_MATURE",
                "allowAnonLogging": False,
                "contentVersion": "1.0.0.0.preview.0",
                "panelizationSummary": {
                    "containsEpubBubbles": False,
                    "containsImageBubbles": False
                },
                "imageLinks": {
                    "smallThumbnail": "http://books.google.com/books/content?id=run-CQAAQBAJ&printsec=frontcover&img=1&zoom=5&source=gbs_api",
                    "thumbnail": "http://books.google.com/books/content?id=run-CQAAQBAJ&printsec=frontcover&img=1&zoom=1&source=gbs_api"
                },
                "language": "pt",
                "previewLink": "http://books.google.com.br/books?id=run-CQAAQBAJ&dq=imita%C3%A7%C3%A3o+de+cristo&hl=&cd=1&source=gbs_api",
                "infoLink": "http://books.google.com.br/books?id=run-CQAAQBAJ&dq=imita%C3%A7%C3%A3o+de+cristo&hl=&source=gbs_api",
                "canonicalVolumeLink": "https://books.google.com/books/about/Imita%C3%A7%C3%A3o_de_Cristo.html?hl=&id=run-CQAAQBAJ"
            },
            "saleInfo": {
                "country": "BR",
                "saleability": "NOT_FOR_SALE",
                "isEbook": False
            },
            "accessInfo": {
                "country": "BR",
                "viewability": "NO_PAGES",
                "embeddable": False,
                "publicDomain": False,
                "textToSpeechPermission": "ALLOWED",
                "epub": {
                    "isAvailable": False
                },
                "pdf": {
                    "isAvailable": False
                },
                "webReaderLink": "http://play.google.com/books/reader?id=run-CQAAQBAJ&hl=&printsec=frontcover&source=gbs_api",
                "accessViewStatus": "NONE",
                "quoteSharingAllowed": False
            },
            "searchInfo": {
                "textSnippet": "Da imitação de Cristo e desprezo de todas as vaidades do mundo 1."
            }
        },

    ]
}
BOOK_DATA_CONTENT_NO_CATEGORIES = {
    "totalItems": 3,
    "items": [
        {"volumeInfo": {
            "title": "Claudii Galeni Opera Omnia",
            "authors": ["Karl Gottlob Kühn"],
            "industryIdentifiers": [
                {
                    "type": "ISBN_13",
                    "identifier": ISBN_13_OK
                },
                {
                    "type": "ISBN_10",
                    "identifier": ISBN_10_OK
                }
            ],
            "imageLinks": {
                "smallThumbnail": "http://books.google.com/books/content?id=raW-65SeIw0C&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api",
                "thumbnail": "http://books.google.com/books/content?id=raW-65SeIw0C&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api"
            },
        }},
        {"volumeInfo": {
            "title": "Claudii Galeni Opera Omnia",
            "authors": ["Karl Gottlob Kühn"],
            "industryIdentifiers": [
                {
                    "type": "ISBN_13",
                    "identifier": ISBN_13_OK
                },
                {
                    "type": "ISBN_10",
                    "identifier": ISBN_10_OK
                }
            ],
            "imageLinks": {
                "smallThumbnail": "http://books.google.com/books/content?id=raW-65SeIw0C&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api",
                "thumbnail": "http://books.google.com/books/content?id=raW-65SeIw0C&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api"
            },
        }},
        {"volumeInfo": {
            "title": "Claudii Galeni Opera Omnia",
            "authors": ["Karl Gottlob Kühn"],
            "industryIdentifiers": [
                {
                    "type": "ISBN_13",
                    "identifier": ISBN_13_OK
                },
                {
                    "type": "ISBN_10",
                    "identifier": ISBN_10_OK
                }
            ],
            "imageLinks": {
                "smallThumbnail": "http://books.google.com/books/content?id=raW-65SeIw0C&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api",
                "thumbnail": "http://books.google.com/books/content?id=raW-65SeIw0C&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api"
            },
        }},
    ]
}

BOOK_DICT = {
    "title": "Claudii Galeni Opera Omnia",
    "authors": ["Karl Gottlob Kühn"],
    'ISBN_13': ISBN_13_OK,
    'ISBN_10': ISBN_10_OK,
    'image_url': "http://books.google.com/books/content?id=raW-65SeIw0C&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api",
    "categories": ["Medical"],
}


class TestSearchBookGoogleApi(TestCase):
    def test_by_isbn_13_ok(self):
        response, status_response = g_api.search_book_google_api(ISBN_13_OK)
        self.assertEqual(status_response, status.HTTP_200_OK)
        self.assertNotEqual(response['totalItems'], 0,
                            'valid isbn_13 must have more than 0 items')

    def test_by_isbn_10_ok(self):
        response, status_response, = g_api.search_book_google_api(ISBN_10_OK)
        self.assertEqual(status_response, status.HTTP_200_OK)
        self.assertNotEqual(response['totalItems'], 0,
                            'valid isbn_10 must have more than 0 items')

    def test_by_isbn_13_bad(self):
        response, status_response = g_api.search_book_google_api(ISBN_13_BAD)
        self.assertEqual(status_response, status.HTTP_200_OK)
        self.assertEqual(response['totalItems'], 0,
                         'invalid isbn_13 must have 0 items')

    def test_by_isbn_10_bad(self):
        response, status_response = g_api.search_book_google_api(ISBN_10_BAD)
        self.assertEqual(status_response, status.HTTP_200_OK)
        self.assertEqual(response['totalItems'], 0,
                         'invalid isbn_10 must have 0 items')

    def test_by_title(self):
        response, status_response = g_api.search_book_google_api('Imitação de Cristo', by_isbn=False)
        self.assertEqual(status_response, status.HTTP_200_OK)


class TestGetSmallThumbnail(TestCase):
    def test_with_image(self):
        small_thumbnail_url = 'url.image.com'
        volume_info = dict(imageLinks=dict(smallThumbnail=small_thumbnail_url))

        image_url = g_api.get_small_thumbnail(volume_info)
        self.assertEqual(image_url, small_thumbnail_url,
                         'get() must have image url')

    def test_with_no_image(self):
        volume_info = {'': ''}
        image_url = g_api.get_small_thumbnail(volume_info)
        self.assertEqual(image_url, '',
                         'get() must have no image url')


class TestGetISBN(TestCase):
    def test_get_isbns_of_volume_info(self):
        volume_info = dict(industryIdentifiers=[
            dict(
                type='ISBN_13',
                identifier=ISBN_13_OK
            ),
            dict(
                type='ISBN_10',
                identifier=ISBN_10_OK
            )
        ])
        isbns = g_api.get_isbns(volume_info)
        self.assertEqual(isbns, (ISBN_13_OK, ISBN_10_OK),
                         'isbns must be equals')


class TestGetBookData(TestCase):
    def setUp(self) -> None:
        self.mock_search_book_google_api = mock.patch(
            'apps.books.search_module.google_api.search_book_google_api').start()
        self.mock_get_small_thumbnail = mock.patch('apps.books.search_module.google_api.get_small_thumbnail').start()
        self.mock_get_isbns = mock.patch('apps.books.search_module.google_api.get_isbns').start()

        self.expected_fields_set = {'title', 'authors', 'ISBN_13', 'ISBN_10', 'categories', 'image_url', 'description'}

    def tearDown(self) -> None:
        self.addCleanup(mock.patch.stopall)

    def test_get_book_by_isbn_true_complete_with_http_200_with_categories(self):
        self.mock_search_book_google_api.return_value = (BOOK_DATA_CONTENT, status.HTTP_200_OK)
        self.mock_get_small_thumbnail.return_value = 'image.url.com'
        self.mock_get_isbns.return_value = (ISBN_13_OK, ISBN_10_OK)

        book, _status = g_api.get_book_data(ISBN_10_OK)
        book_fields = set(book.keys())

        self.assertEqual(book_fields, self.expected_fields_set, 'fields received must be equal')
        self.assertEqual(book['ISBN_13'], ISBN_13_OK)
        self.assertEqual(book['ISBN_10'], ISBN_10_OK)

    def test_get_book_by_isbn_true_complete_with_http_200_with_no_categories(self):
        self.mock_get_isbns.return_value = (ISBN_13_OK, ISBN_10_OK)
        self.mock_get_small_thumbnail.return_value = 'image.url.com'
        self.mock_search_book_google_api.return_value = (BOOK_DATA_CONTENT_NO_CATEGORIES, status.HTTP_200_OK)

        book, _status = g_api.get_book_data(ISBN_10_OK)
        book_fields = set(book.keys())
        self.assertEqual(book_fields, self.expected_fields_set, 'fields received must be equal')
        self.assertEqual(book['categories'], '', 'categories must be "" ')
        self.assertEqual(book['ISBN_13'], ISBN_13_OK)
        self.assertEqual(book['ISBN_10'], ISBN_10_OK)

    def test_get_book_by_isbn_true_complete_with_http_not_200(self):
        self.mock_get_isbns.return_value = (ISBN_13_OK, ISBN_10_OK)
        self.mock_get_small_thumbnail.return_value = 'image.url.com'
        self.mock_search_book_google_api.return_value = ([], False)

        book, _status = g_api.get_book_data(ISBN_10_OK)
        self.assertEqual(book, [],
                         'book must be []')

    def test_get_book_qty_many_complete_with_http_200_with_categories(self):
        self.mock_search_book_google_api.return_value = (BOOK_DATA_CONTENT, status.HTTP_200_OK)
        self.mock_get_small_thumbnail.return_value = 'image.url.com'
        self.mock_get_isbns.return_value = (ISBN_13_OK, ISBN_10_OK)

        books, _status = g_api.get_book_data(value="title", by_isbn=False)

        self.assertEqual(len(books), BOOK_DATA_CONTENT["totalItems"])

        book_0_fields = set(books[0].keys())
        self.assertEqual(book_0_fields, self.expected_fields_set, 'fields received must be equal')
        self.assertEqual(books[0]['ISBN_13'], ISBN_13_OK)
        self.assertEqual(books[0]['ISBN_10'], ISBN_10_OK)

        book_0_fields = set(books[2].keys())
        self.assertEqual(book_0_fields, self.expected_fields_set, 'fields received must be equal')
        self.assertEqual(books[2]['ISBN_13'], ISBN_13_OK)
        self.assertEqual(books[2]['ISBN_10'], ISBN_10_OK)

    def test_get_book_qty_many_complete_with_http_200_with_no_categories(self):
        self.mock_get_isbns.return_value = (ISBN_13_OK, ISBN_10_OK)
        self.mock_get_small_thumbnail.return_value = 'image.url.com'
        self.mock_search_book_google_api.return_value = (BOOK_DATA_CONTENT_NO_CATEGORIES, status.HTTP_200_OK)

        books, _status = g_api.get_book_data(value="Title", by_isbn=False)

        self.assertEqual(len(books), BOOK_DATA_CONTENT_NO_CATEGORIES["totalItems"])

        book_fields = set(books[0].keys())
        self.assertEqual(book_fields, self.expected_fields_set, 'fields received must be equal')
        self.assertEqual(books[0]['categories'], '', 'categories must be "" ')
        self.assertEqual(books[0]['ISBN_13'], ISBN_13_OK)
        self.assertEqual(books[0]['ISBN_10'], ISBN_10_OK)

        book_fields = set(books[2].keys())
        self.assertEqual(book_fields, self.expected_fields_set, 'fields received must be equal')
        self.assertEqual(books[2]['categories'], '', 'categories must be "" ')
        self.assertEqual(books[2]['ISBN_13'], ISBN_13_OK)
        self.assertEqual(books[2]['ISBN_10'], ISBN_10_OK)

    def test_get_book_qty_many_complete_with_http_not_200(self):
        self.mock_get_isbns.return_value = (ISBN_13_OK, ISBN_10_OK)
        self.mock_get_small_thumbnail.return_value = 'image.url.com'
        self.mock_search_book_google_api.return_value = ([], False)

        books, _status = g_api.get_book_data(value="title", by_isbn=False)
        self.assertEqual(books, [], 'books must be []')
