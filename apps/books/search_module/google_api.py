import json
import urllib.request
from http import HTTPStatus


def search_book_google_api(value, by_isbn=True):
    search_url = "https://www.googleapis.com/books/v1/volumes?q="

    if by_isbn:
        search_url += "isbn:" + value
    else:
        search_url += urllib.parse.quote(value)

    with urllib.request.urlopen(search_url, ) as response:
        data = json.loads(response.read().decode('utf-8'))
    return data, response.code


def get_small_thumbnail(volume_info):
    image_links = volume_info.get('imageLinks', None)
    if image_links is None:
        return ""
    return image_links['smallThumbnail']


def get_isbns(volume_info):
    isbns = {}
    industry_identifiers = volume_info.get("industryIdentifiers", None)

    if industry_identifiers:
        for isbn in industry_identifiers:
            if 'ISBN' in isbn['type']:
                isbns[isbn['type']] = isbn['identifier']
            else:
                continue
    return isbns.get('ISBN_13', ""), isbns.get('ISBN_10', "")


def get_book_data(value, by_isbn=True):
    books = []
    data_content, status = search_book_google_api(value=value, by_isbn=by_isbn)

    if status == HTTPStatus.OK:
        qty = 1 if by_isbn else len(data_content['items'])

        for index in range(qty):
            book = {}
            volume_info = data_content['items'][index].get('volumeInfo', None)
            if volume_info:
                book['title'] = volume_info['title']
                book['authors'] = volume_info.get('authors', "")
                book['description'] = volume_info.get('description')

                book['ISBN_13'], book['ISBN_10'] = get_isbns(volume_info)

                book['categories'] = volume_info.get('categories', None)
                if book['categories'] is None:
                    book['categories'] = ""
                book['image_url'] = get_small_thumbnail(volume_info)
                books.append(book)
        if by_isbn:
            return books[0], status
        return books, status

    else:
        return books, status
