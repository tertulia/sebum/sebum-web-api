from rest_framework.serializers import ModelSerializer

from ..models import Book, Author, Category


class AuthorSerializer(ModelSerializer):
    class Meta:
        model = Author
        fields = ["name"]
        extra_kwargs = {
            "name": {"read_only": True}
        }


class CategorySerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = ["name"]
        extra_kwargs = {
            "name": {"read_only": True}
        }


class BookSerializer(ModelSerializer):
    categories = CategorySerializer(many=True)
    authors = AuthorSerializer(many=True)

    class Meta:
        model = Book
        fields = ["title", "authors", "image_url", "categories"]
        extra_kwargs = {
            "title": {"read_only": True},
            "authors": {"read_only": True},
            "image_url": {"read_only": True}
        }
