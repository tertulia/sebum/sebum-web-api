from django.urls import path

from apps.books import views

urlpatterns = [
    path('search-books', views.search_book, name='search_books'),
]
