from django.db import models

from apps.books.search_module import google_api


class Author(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class BookManager(models.Manager):
    # excluded many_to_many fields
    books_field = ['ISBN_10', 'ISBN_13', 'title', 'image_url']

    def create_with_data(self, book_data: dict):
        payload_to_save = dict()
        for field, value in book_data.items():
            if field in self.books_field and value:
                payload_to_save[field] = value

        book = self.create(**payload_to_save)
        authors = book_data.get('authors', '')
        categories = book_data.get('categories', '')

        for author in authors:
            author, created = Author.objects.get_or_create(name=author)
            author.save()
            book.authors.add(author)
        for category in categories:
            category, created = Category.objects.get_or_create(name=category)
            category.save()
            book.categories.add(category)
            book.save()
        book.save()
        return book

    def create_with_isbn(self, isbn):
        book_dict, response_status_api = google_api.get_book_data(isbn)
        if response_status_api != 200:
            return None
        else:
            book = self.create_with_data(book_dict)
            return book


class Book(models.Model):
    ISBN_10 = models.CharField(max_length=20, null=True, blank=True)
    ISBN_13 = models.CharField(max_length=20, null=True, blank=True)
    title = models.CharField(max_length=100)
    authors = models.ManyToManyField(Author)
    categories = models.ManyToManyField(Category)
    image_url = models.CharField(max_length=255, null=True, blank=True)

    objects = BookManager()

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.title = self.title.lower()
        return super(Book, self).save(*args, **kwargs)

    def euclidean_other_book(self, other):
        from math import sqrt
        from apps.bookshelves.models import BookShelf

        if self.pk == other.pk:
            return 1

        book_shelf = BookShelf.objects.filter(book=self.pk)
        users_that_have_shelf = book_shelf.values("owner")

        other_shelf = BookShelf.objects.filter(book=other.pk)
        other_shelf_owners = other_shelf.values("owner").distinct()

        _sum = 0
        for book_in_shelf, owners_name in zip(book_shelf, users_that_have_shelf):
            if owners_name in other_shelf_owners:
                other_book = other_shelf.get(owner_id=owners_name["owner"])
                _sum += pow(book_in_shelf.evaluation - other_book.evaluation, 2)

        return 1 / (1 + sqrt(_sum)) if _sum != 0 else _sum

    def get_similarities(self, num=False):
        similarities = []
        for other_book in Book.objects.exclude(pk=self.pk):
            euclidean = self.euclidean_other_book(other_book)
            if euclidean != 0:
                similarities.append((euclidean, other_book))
        similarities.sort(key=lambda simi: simi[0], reverse=True)

        if num:
            return similarities[:num]

        return similarities

    def get_all_authors_as_one_string(self):
        return ", ".join([author.name.title() for author in self.authors.all()])

    def get_all_categories_as_one_string(self):
        return ", ".join([category.name.title() for category in self.categories.all()])
