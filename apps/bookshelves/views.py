import ast

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.shortcuts import redirect, get_object_or_404
from django.views.generic import ListView

from apps.books.models import Book
from apps.bookshelves.models import SharedBookShelves, BookShelf


class SharedBookShelvesBaseListView(LoginRequiredMixin, ListView):
    model = SharedBookShelves
    redirect_field_name = 'login'
    status_book = -1

    def get_queryset(self):
        return self.model.objects.filter(
            (Q(solicitor=self.request.user) | Q(bookshelf__owner=self.request.user)
             ) & Q(status=self.status_book)).order_by('date_requested')


class SharedBookShelvesRequestedListView(SharedBookShelvesBaseListView):
    template_name = 'bookshelves/requested.html'
    status_book = 0


class SharedBookShelvesSharedListView(SharedBookShelvesBaseListView):
    template_name = 'bookshelves/shared.html'
    status_book = 1


class SharedBookShelvesReturnedListView(SharedBookShelvesBaseListView):
    template_name = 'bookshelves/returned.html'
    status_book = 2


class SharedBookShelvesDeniedListView(SharedBookShelvesBaseListView):
    template_name = 'bookshelves/denied.html'
    status_book = 3


class BookShelvesListView(LoginRequiredMixin, ListView):
    template_name = 'bookshelves/bookshelves.html'
    redirect_field_name = 'login'
    paginate_by = 10
    model = BookShelf

    def get_queryset(self):
        return self.model.objects.filter(owner=self.request.user).order_by('available')


@login_required(login_url='login')
def create_bookshelf(request):
    if request.method == 'POST':
        book_data = ast.literal_eval(request.POST["book_data"])
        book = Book.objects.create_with_data(book_data)
        BookShelf.objects.create(owner=request.user, book=book)
        messages.success(request, "livro adicionado a estante")

    return redirect('bookshelves')


@login_required(login_url='login')
def evaluate_bookshelf(request, pk):
    if request.method == 'POST':
        bookshelf = get_object_or_404(BookShelf, pk=pk, owner=request.user)
        evaluation = int(request.POST['evaluation'])
        if 0 <= evaluation <= 10:
            bookshelf.evaluation = evaluation
            bookshelf.save()
            messages.success(request, 'Livro avaliado com sucesso')
        else:
            messages.warning(request, 'Avaliação precisa ser um número entre 0 e 10')

    return redirect('bookshelves')
