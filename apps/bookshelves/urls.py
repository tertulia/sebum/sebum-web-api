from django.urls import path

from . import views

urlpatterns = [
    path('requested', views.SharedBookShelvesRequestedListView.as_view(), name='bookshelves-requested'),
    path('shared', views.SharedBookShelvesSharedListView.as_view(), name='bookshelves-shared'),
    path('returned', views.SharedBookShelvesReturnedListView.as_view(), name='bookshelves-returned'),
    path('denied', views.SharedBookShelvesDeniedListView.as_view(), name='bookshelves-denied'),
    path('', views.BookShelvesListView.as_view(), name='bookshelves'),
    path('create-bookshelf', views.create_bookshelf, name='create-bookshelf'),
    path('evaluate-bookshelf/<int:pk>', views.evaluate_bookshelf, name='evaluate-bookshelf'),
]
