from django.db import models
from django.db.models import Q
from django.utils.datetime_safe import date

from apps.books.models import Book
from sebum_web_api import settings


class BookShelfManager(models.Manager):
    def create_with_isbn(self, isbn, user):

        book = Book.objects.filter(Q(ISBN_10=isbn) | Q(ISBN_13=isbn)).first()
        if book is not None:
            existing_bookshelf = self.filter(book=book, owner=user).first()
            if existing_bookshelf:
                return existing_bookshelf
            bookshelf = self.create(owner=user, book=book)
            bookshelf.save()
            return bookshelf

        book = Book.objects.create_with_isbn(isbn)
        if book is not None:
            bookshelf = self.create(owner=user, book=book)
            bookshelf.save()
            return bookshelf
        return None


class BookShelf(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    evaluation = models.IntegerField(null=True, blank=True)
    available = models.BooleanField(default=True)

    objects = BookShelfManager()

    def __str__(self):
        return f"BookShelf of {self.owner}"


class SharedBookShelves(models.Model):
    solicitor = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    bookshelf = models.ForeignKey(BookShelf, on_delete=models.CASCADE)
    date_requested = models.DateField(default=date.today)
    date_returned = models.DateField(null=True, blank=True)
    STATUS = [
        (0, 'solicitado'),
        (1, 'emprestado'),
        (2, 'retornado'),
        (3, 'negado')
    ]
    status = models.SmallIntegerField(choices=STATUS, default=0)
    evaluation = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return f"'{self.bookshelf.owner}' borrow '{self.bookshelf.book.title}' to '{self.solicitor}' "
