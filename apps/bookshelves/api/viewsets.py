from django.db.models import Q
from rest_framework import filters
from rest_framework import mixins, viewsets
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .permissions import UpdateOwnBookShelf
from .serializers import BookShelfSerializer, SharedBookShelfSerializer
from ..models import BookShelf, SharedBookShelves


class BookShelvesViewSet(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication,)
    serializer_class = BookShelfSerializer
    queryset = BookShelf.objects.all()
    permission_classes = (IsAuthenticated, UpdateOwnBookShelf)
    filter_backends = (filters.SearchFilter,)
    search_fields = ('available', 'owner__id', '^book__title')

    def create(self, request, *args, **kwargs):
        isbn = request.data['isbn']
        if len(isbn) == 13 or len(isbn) == 10:
            user = self.request.user
            bookshelf = BookShelf.objects.create_with_isbn(isbn, user)

            if bookshelf is None:
                return Response({"message": "Ops... Algo de errado não está certo! Tente mais tarde"},
                                status=status.HTTP_404_NOT_FOUND)
            serializer = BookShelfSerializer(bookshelf)
            return Response({"message": f"success! {bookshelf.book.title} adicionado a estente",
                             "bookshelf": serializer.data}, status=status.HTTP_201_CREATED)
        return Response({"message": "ISBN inválido. ISBN precisa 13 ou 10 dígitos"},
                        status=status.HTTP_400_BAD_REQUEST)


class BookShelfViewSet(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication,)
    serializer_class = BookShelfSerializer
    queryset = BookShelf.objects.all()
    permission_classes = (IsAuthenticated, UpdateOwnBookShelf)

    def create(self, request, *args, **kwargs):
        isbn = request.data['isbn']
        if len(isbn) == 13 or len(isbn) == 10:
            user = self.request.user
            bookshelf = BookShelf.objects.create_with_isbn(isbn, user)

            if bookshelf is None:
                return Response({"message": "Ops... Algo de errado não está certo! Tente mais tarde"},
                                status=status.HTTP_404_NOT_FOUND)
            serializer = BookShelfSerializer(bookshelf)
            return Response({"message": f"success! {bookshelf.book.title} adicionado a estente",
                             "bookshelf": serializer.data}, status=status.HTTP_201_CREATED)
        return Response({"message": "ISBN inválido. ISBN precisa 13 ou 10 dígitos"},
                        status=status.HTTP_400_BAD_REQUEST)


class SharedBookShelfViewSet(viewsets.GenericViewSet, mixins.CreateModelMixin, mixins.UpdateModelMixin):
    serializer_class = SharedBookShelfSerializer
    queryset = SharedBookShelves.objects.all()
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    status_shared_bookshelf = {'requested': '0',
                               'borrowed': '1',
                               'returned': '2',
                               'denied': '3',
                               }

    def get_queryset(self):
        return self.queryset.filter(Q(solicitor=self.request.user) | Q(bookshelf__owner=self.request.user))

    def create(self, request, *args, **kwargs):
        bookshelf = BookShelf.objects.get(id=self.request.data['bookshelf'])
        borrowed_bookshelf = self.get_queryset().filter(bookshelf=bookshelf)
        if borrowed_bookshelf.exists():
            denied = 3
            returned = 2
            if borrowed_bookshelf.exclude(status=denied).exclude(status=returned).exists():
                return Response({'message': "Solicitação de livro em aberto"}, status=status.HTTP_403_FORBIDDEN)
            serializer = self.get_serializer(data=self.request.data)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)

            return Response({"message": "Livro solicitado com sucesso", **serializer.data},
                            status=status.HTTP_201_CREATED)

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return Response({"message": "Livro solicitado com sucesso", **serializer.data},
                        status=status.HTTP_201_CREATED)

    def perform_create(self, serializer):
        """Create new BorrowedBoolShelf"""
        serializer.save(solicitor=self.request.user)

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)

    def perform_update(self, serializer):
        serializer.save()

    @action(detail=True, methods=['patch'])
    def borrow(self, request, pk=None):
        instance = self.get_object()
        if self.request.user == instance.solicitor:
            instance.bookshelf.available = False
            instance.bookshelf.save()
            serializer = self.get_serializer(instance, data={'status': self.status_shared_bookshelf['borrowed']},
                                             partial=True)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            return Response(serializer.data)

        return Response({"message": "Você não pode alterar para esse status"}, status=status.HTTP_403_FORBIDDEN)

    @action(detail=True, methods=['patch'], url_name='return')
    def return_(self, request, pk=None):
        instance = self.get_object()
        if self.request.user == instance.bookshelf.owner:
            instance.bookshelf.available = True
            instance.bookshelf.save()
            serializer = self.get_serializer(instance, data={'status': self.status_shared_bookshelf['returned']},
                                             partial=True)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            return Response(serializer.data)

        return Response({"message": "Você não pode alterar para esse status"}, status=status.HTTP_403_FORBIDDEN)

    @action(detail=True, methods=['patch'])
    def deny(self, request, pk=None):
        instance = self.get_object()
        if self.request.user == instance.bookshelf.owner:
            serializer = self.get_serializer(instance, data={'status': self.status_shared_bookshelf['denied']},
                                             partial=True)
            serializer.is_valid(raise_exception=False)
            self.perform_update(serializer)
            return Response(serializer.data)

        return Response({"message": "Você não pode alterar para esse status"}, status=status.HTTP_403_FORBIDDEN)

    @action(detail=False)
    def borrowed(self, request, pk=None):
        serializer = SharedBookShelfSerializer(self.queryset.filter(bookshelf__owner=self.request.user), many=True)
        return Response(serializer.data)

    @action(detail=False)
    def solicited(self, request, pk=None):
        serializer = SharedBookShelfSerializer(self.queryset.filter(solicitor=self.request.user), many=True)
        return Response(serializer.data)
