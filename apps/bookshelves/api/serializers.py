from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from ..models import BookShelf, SharedBookShelves
from ...books.api.serializers import BookSerializer


class BookShelfSerializer(ModelSerializer):
    book = BookSerializer()

    class Meta:
        model = BookShelf
        fields = ['owner', 'book', 'evaluation', 'available', 'id']
        extra_kwargs = {
            "owner": {"read_only": True}
        }


class SharedBookShelfSerializer(ModelSerializer):
    bookshelf = serializers.PrimaryKeyRelatedField(queryset=BookShelf.objects.all())
    book = serializers.SerializerMethodField()
    owner = serializers.SerializerMethodField()

    class Meta:
        model = SharedBookShelves
        fields = ['id', 'solicitor', 'bookshelf', 'book', 'owner', 'date_requested', 'date_returned', 'status',
                  'evaluation']
        extra_kwargs = {
            "solicitor": {"read_only": True},
            "bookshelf": {"read_only": True},
            "date_requested": {"read_only": True},
            "date_returned": {"read_only": True},
        }

    def get_book(self, obj):
        return obj.bookshelf.book.title

    def get_owner(self, obj):
        return obj.bookshelf.owner.email
