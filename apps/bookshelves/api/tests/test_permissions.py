import random
from unittest import TestCase, mock

from apps.bookshelves.api.permissions import UpdateOwnBookShelf


class TestBookShelfPermissions(TestCase):
    def setUp(self) -> None:
        self.permission = UpdateOwnBookShelf()
        self.view = mock.MagicMock()
        self.obj = mock.MagicMock()
        self.request = mock.MagicMock()
        self.safe_method = ('GET', 'HEAD', 'OPTIONS')

    def test_has_object_have_safe_permission(self):
        self.request.method = random.choice(self.safe_method)
        self.assertTrue(self.permission.has_object_permission(self.request, self.view, self.obj))

    def test_has_object_have_no_safe_permission_but_is_owner(self):
        self.request.method = 'NO PERMISSION'
        self.obj.owner.id = self.request.user.id = 1
        self.assertTrue(self.permission.has_object_permission(self.request, self.view, self.obj))
