from django.contrib import admin

from .models import BookShelf, SharedBookShelves


class BookShelfAdmin(admin.ModelAdmin):
    list_display = ["owner", "book", "evaluation", "available"]


class SharedBookShelvesAdmin(admin.ModelAdmin):
    list_display = ["solicitor", "book", "owner", "date_requested", "date_returned", "status"]

    def book(self, obj):
        return obj.bookshelf.book

    def owner(self, obj):
        return obj.bookshelf.owner


admin.site.register(BookShelf, BookShelfAdmin)
admin.site.register(SharedBookShelves, SharedBookShelvesAdmin)
