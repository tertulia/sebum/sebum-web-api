import random
from unittest import mock

from django.test import TestCase

from apps.accounts.models import User
from apps.books.models import Book, Author, Category
from ..models import BookShelf, SharedBookShelves


class TestModelBookShelf(TestCase):
    def setUp(self) -> None:
        num_of_models = 3
        for _id in range(num_of_models):
            user = User.objects.create_user(
                email=f'user_{_id}@email.com',
                name=f'complete user_{_id} name',
            )

            author = Author.objects.create(name=f'This author_{_id}')
            category = Category.objects.create(name=f'some category_{_id}')

            book = Book.objects.create(
                ISBN_10=f"{_id}",
                ISBN_13=f"{_id}_{_id}",
                title=f'Some book_{_id}',
                image_url=f'imagebook_{_id}.url.com'
            )
            book.authors.add(author)
            book.categories.add(category)
            book.save()
            BookShelf.objects.create(
                owner=user,
                book=book,
                evaluation=random.randint(1, 10),
                available=random.choice([True, False])
            )

    def test_string_representation(self):
        bookshelf = BookShelf.objects.get(id=1)
        self.assertEqual(str(bookshelf), f'BookShelf of {bookshelf.owner}',
                         '__str__ of book must be "Bookshelf of owner_name"')

    def test_if_deletion_of_book_delete_bookshelf(self):
        _id = 1
        Book.objects.get(id=_id).delete()
        self.assertTrue(not BookShelf.objects.filter(id=_id), 'bookshelf must be deleted with book')

    def test_if_deletion_of_user_delete_bookshelf(self):
        _id = 2
        User.objects.get(id=_id).delete()
        self.assertTrue(not BookShelf.objects.filter(id=_id), 'bookshelf must be deleted with owner')

    def test_create_bookshelf_by_isbn_book_does_not_exist_then_its_created(self):
        with mock.patch('apps.bookshelves.models.Book.objects.create_with_isbn',
                        return_value=Book.objects.get(id=1)):
            isbn = 'not_existent'
            user = User.objects.create_user('user@email.com.br', 'user name', 'his_pass')
            bookshelf = BookShelf.objects.create_with_isbn(isbn, user)
            self.assertEqual(bookshelf.book.id, 1, 'id of book in bookshelf must be same as mock')

    def test_create_bookshelf_by_isbn_fail_by_invalid_isbn(self):
        with mock.patch('apps.bookshelves.models.Book.objects.create_with_isbn', return_value=None):
            isbn = 'invalidisbn'
            user = User.objects.get(id=1)
            bookshelf = BookShelf.objects.create_with_isbn(isbn, user)
            self.assertEqual(bookshelf, None, 'fail creating must return None')

    def test_owner_create_same_bookshelf_by_isbn(self):
        bookshelf = BookShelf.objects.get(id=1)
        owner = bookshelf.owner
        isbn = bookshelf.book.ISBN_10
        same_bookshelf = BookShelf.objects.create_with_isbn(isbn, owner)
        self.assertEqual(bookshelf.id, same_bookshelf.id,
                         'id must be the same (not creating new bookshelf)')
        self.assertEqual(bookshelf.owner, same_bookshelf.owner,
                         'owners must be the same')

    def test_create_bookshelf_by_isbn_existent_book_success(self):
        user = User.objects.get(id=1)
        book = Book.objects.create(
            ISBN_10="someisbn",
            ISBN_13="someisbn",
            title="book title",
            image_url='image.url.com    '
        )
        isbn = book.ISBN_10

        bookshelf = BookShelf.objects.create_with_isbn(isbn, user)
        self.assertEqual(bookshelf.book, book, 'book in shelf must be equal book')
        self.assertEqual(bookshelf.owner, user, 'owner in shelf must be equal user')


class TestModelSharedBookShelf(TestCase):
    def setUp(self) -> None:
        num_of_models = 3
        for _id in range(num_of_models):
            user = User.objects.create_user(
                email=f'user_{_id}@email.com',
                name=f'complete user_{_id} name',
                bio=f'a nice bio for user_{_id}',
            )

            author = Author.objects.create(name=f'This author_{_id}')
            category = Category.objects.create(name=f'some category_{_id}')

            book = Book.objects.create(
                ISBN_10=f"{_id}",
                ISBN_13=f"{_id}_{_id}",
                title=f'Some book_{_id}',
                image_url=f'imagebook_{_id}.url.com'
            )
            book.authors.add(author)
            book.categories.add(category)
            book.save()
            BookShelf.objects.create(
                owner=user,
                book=book,
                evaluation=random.randint(1, 10),
                available=random.choice([True, False])
            )

        SharedBookShelves.objects.create(
            solicitor=User.objects.get(id=1),
            bookshelf=BookShelf.objects.get(id=2)
        )

    def test_string_representation(self):
        borrowed_bookshelf = SharedBookShelves.objects.get(id=1)
        self.assertEqual(str(borrowed_bookshelf),
                         f"'{borrowed_bookshelf.bookshelf.owner}' borrow '{borrowed_bookshelf.bookshelf.book.title}' to '{borrowed_bookshelf.solicitor}' ",
                         '__str__ of book must be the owner, the title of the book and solicitor')

    def test_if_deletion_of_book_delete_shared_bookshelf(self):
        _id = 1
        Book.objects.get(id=_id).delete()
        self.assertTrue(not SharedBookShelves.objects.filter(bookshelf__book_id=_id),
                        'borrowed_bookshelf must be deleted with book')

    def test_if_deletion_of_solicitor_delete_shared_bookshelf(self):
        _id = 1
        User.objects.get(id=_id).delete()
        self.assertTrue(not SharedBookShelves.objects.filter(solicitor_id=_id),
                        'borrowed_bookshelf must be deleted with solicitor')

    def test_if_deletion_of_bookshelf_owner_delete_shared_bookshelf(self):
        _id = 2
        User.objects.get(id=_id).delete()
        self.assertTrue(not SharedBookShelves.objects.filter(bookshelf__owner_id=_id),
                        'borrowed_bookshelf must be deleted with bookshelf owner')
