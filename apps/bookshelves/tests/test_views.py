import ast

from django.contrib.auth import get_user_model
from django.test import TestCase, Client
from django.urls import reverse

from apps.books.models import Book
from apps.bookshelves.models import SharedBookShelves, BookShelf


class BaseConfigTest(TestCase):
    def setUp(self) -> None:
        self.client = Client()
        self.user = get_user_model().objects.create_user(email='user@email.com',
                                                         name='user name',
                                                         password="some pass", )
        self.other_user = get_user_model().objects.create_user(email='other@email.com',
                                                               name='user name',
                                                               password="some pass", )
        self.client.force_login(self.user)

    def sample_book(self, title="a good book"):
        return Book.objects.create(title=title)

    def sample_bookshelf(self, owner, book=None):
        if book:
            return BookShelf.objects.create(
                owner=owner,
                book=book
            )
        return BookShelf.objects.create(
            owner=owner,
            book=self.sample_book()
        )

    def sample_shared_bookshelf(self, solicitor, bookshelf, status):
        _status = dict(
            requested=0,
            shared=1,
            returned=2,
            denied=3)

        return SharedBookShelves.objects.create(
            solicitor=solicitor,
            bookshelf=bookshelf,
            status=_status[status]
        )


class TestBookshelvesRequestedListView(BaseConfigTest):
    bookshelves_requested_url = reverse('bookshelves-requested')

    def test_unauthenticated_client_redirect_to_login(self):
        unauthenticated_client = Client()
        response = unauthenticated_client.get(self.bookshelves_requested_url)
        self.assertRedirects(response, '/accounts/login/?login=' + self.bookshelves_requested_url)

    def test_bookshelf_requested_templates_used(self):
        bs = self.sample_bookshelf(owner=self.other_user)
        self.sample_shared_bookshelf(self.user, bs, "requested")
        response = self.client.get(self.bookshelves_requested_url)
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'bookshelves/requested.html')
        self.assertTemplateUsed(response, 'bookshelves/partials/shared_bookshelf_widget.html')

    def test_requested_bookshelf_in_context(self):
        bookshelf = self.sample_bookshelf(owner=self.other_user)
        sharad_bookshelf = self.sample_shared_bookshelf(self.user, bookshelf, "requested")
        response = self.client.get(self.bookshelves_requested_url)
        self.assertIn(sharad_bookshelf, response.context["object_list"])

    def test_only_requested_bookshelf_in_context(self):
        bookshelf = self.sample_bookshelf(owner=self.other_user)
        shared_bookshelf_shared = self.sample_shared_bookshelf(self.user, bookshelf, "shared")
        shared_bookshelf_returned = self.sample_shared_bookshelf(self.user, bookshelf, "returned")
        shared_bookshelf_denied = self.sample_shared_bookshelf(self.user, bookshelf, "denied")

        response = self.client.get(self.bookshelves_requested_url)
        self.assertNotIn(shared_bookshelf_shared, response.context["object_list"])
        self.assertNotIn(shared_bookshelf_returned, response.context["object_list"])
        self.assertNotIn(shared_bookshelf_denied, response.context["object_list"])


class TestBookshelvesSharedListView(BaseConfigTest):
    bookshelves_shared_url = reverse('bookshelves-shared')

    def test_unauthenticated_client_redirect_to_login(self):
        unauthenticated_client = Client()
        response = unauthenticated_client.get(self.bookshelves_shared_url)
        self.assertRedirects(response, '/accounts/login/?login=' + self.bookshelves_shared_url)

    def test_bookshelves_shared_templates_used(self):
        bs = self.sample_bookshelf(owner=self.other_user)
        self.sample_shared_bookshelf(self.user, bs, "shared")
        response = self.client.get(self.bookshelves_shared_url)
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'bookshelves/shared.html')
        self.assertTemplateUsed(response, 'bookshelves/partials/shared_bookshelf_widget.html')

    def test_shared_bookshelves_in_context(self):
        bookshelf = self.sample_bookshelf(owner=self.other_user)
        shared_bookshelf = self.sample_shared_bookshelf(self.user, bookshelf, "shared")
        response = self.client.get(self.bookshelves_shared_url)
        self.assertIn(shared_bookshelf, response.context['object_list'])

    def test_only_shared_bookshelf_in_context(self):
        bookshelf = self.sample_bookshelf(owner=self.other_user)
        shared_bookshelf_requested = self.sample_shared_bookshelf(self.user, bookshelf, "requested")
        shared_bookshelf_returned = self.sample_shared_bookshelf(self.user, bookshelf, "returned")
        shared_bookshelf_denied = self.sample_shared_bookshelf(self.user, bookshelf, "denied")

        response = self.client.get(self.bookshelves_shared_url)
        self.assertNotIn(shared_bookshelf_requested, response.context["object_list"])
        self.assertNotIn(shared_bookshelf_returned, response.context["object_list"])
        self.assertNotIn(shared_bookshelf_denied, response.context["object_list"])


class TestBookshelvesReturnedListView(BaseConfigTest):
    bookshelves_returned_url = reverse('bookshelves-returned')

    def test_unauthenticated_client_redirect_to_login(self):
        unauthenticated_client = Client()
        response = unauthenticated_client.get(self.bookshelves_returned_url)
        self.assertRedirects(response, '/accounts/login/?login=' + self.bookshelves_returned_url)

    def test_bookshelves_returned_templates_used(self):
        bs = self.sample_bookshelf(owner=self.other_user)
        self.sample_shared_bookshelf(self.user, bs, "returned")
        response = self.client.get(self.bookshelves_returned_url)
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'bookshelves/returned.html')
        self.assertTemplateUsed(response, 'bookshelves/partials/shared_bookshelf_widget.html')

    def test_returned_bookshelves_in_context(self):
        bookshelf = self.sample_bookshelf(owner=self.other_user)
        shared_bookshelf = self.sample_shared_bookshelf(self.user, bookshelf, "returned")
        response = self.client.get(self.bookshelves_returned_url)
        self.assertIn(shared_bookshelf, response.context['object_list'])

    def test_only_returned_bookshelf_in_context(self):
        bookshelf = self.sample_bookshelf(owner=self.other_user)
        shared_bookshelf_requested = self.sample_shared_bookshelf(self.user, bookshelf, "requested")
        shared_bookshelf_shared = self.sample_shared_bookshelf(self.user, bookshelf, "shared")
        shared_bookshelf_denied = self.sample_shared_bookshelf(self.user, bookshelf, "denied")

        response = self.client.get(self.bookshelves_returned_url)
        self.assertNotIn(shared_bookshelf_requested, response.context["object_list"])
        self.assertNotIn(shared_bookshelf_shared, response.context["object_list"])
        self.assertNotIn(shared_bookshelf_denied, response.context["object_list"])


class TestBookshelvesDeniedListView(BaseConfigTest):
    bookshelves_denied_url = reverse('bookshelves-denied')

    def test_unauthenticated_client_redirect_to_login(self):
        unauthenticated_client = Client()
        response = unauthenticated_client.get(self.bookshelves_denied_url)
        self.assertRedirects(response, '/accounts/login/?login=' + self.bookshelves_denied_url)

    def test_bookshelves_denied_templates_used(self):
        bs = self.sample_bookshelf(owner=self.other_user)
        self.sample_shared_bookshelf(self.user, bs, "denied")
        response = self.client.get(self.bookshelves_denied_url)
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'bookshelves/denied.html')
        self.assertTemplateUsed(response, 'bookshelves/partials/shared_bookshelf_widget.html')

    def test_denied_bookshelves_in_context(self):
        bookshelf = self.sample_bookshelf(owner=self.other_user)
        shared_bookshelf = self.sample_shared_bookshelf(self.user, bookshelf, "denied")
        response = self.client.get(self.bookshelves_denied_url)
        self.assertIn(shared_bookshelf, response.context['object_list'])

    def test_only_denied_bookshelf_in_context(self):
        bookshelf = self.sample_bookshelf(owner=self.other_user)
        shared_bookshelf_requested = self.sample_shared_bookshelf(self.user, bookshelf, "requested")
        shared_bookshelf_shared = self.sample_shared_bookshelf(self.user, bookshelf, "shared")
        shared_bookshelf_returned = self.sample_shared_bookshelf(self.user, bookshelf, "returned")

        response = self.client.get(self.bookshelves_denied_url)
        self.assertNotIn(shared_bookshelf_requested, response.context["object_list"])
        self.assertNotIn(shared_bookshelf_shared, response.context["object_list"])
        self.assertNotIn(shared_bookshelf_returned, response.context["object_list"])


class TestBookshelvesListView(BaseConfigTest):
    bookshelves_url = reverse('bookshelves')

    def test_unauthenticated_client_redirect_to_login(self):
        unauthenticated_client = Client()
        response = unauthenticated_client.get(self.bookshelves_url)
        self.assertRedirects(response, '/accounts/login/?login=' + self.bookshelves_url)

    def test_bookshelves_templates_used(self):
        self.sample_bookshelf(owner=self.user)
        response = self.client.get(self.bookshelves_url)
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'bookshelves/bookshelves.html')
        self.assertTemplateUsed(response, 'bookshelves/partials/bookshelf_widget.html')

    def test_bookshelves_in_context(self):
        bookshelf = self.sample_bookshelf(owner=self.user)
        response = self.client.get(self.bookshelves_url)
        self.assertIn(bookshelf, response.context['page_obj'])

    def test_only_owner_bookshelf_in_context(self):
        other_owner_bookshelf = self.sample_bookshelf(owner=self.other_user)
        owner_bookshelf = self.sample_bookshelf(owner=self.user)

        response = self.client.get(self.bookshelves_url)
        self.assertNotIn(other_owner_bookshelf, response.context["page_obj"])
        self.assertIn(owner_bookshelf, response.context["page_obj"])


class TestCreateBookshelfView(BaseConfigTest):
    create_bookshelf_url = reverse('create-bookshelf')

    def test_unauthenticated_client_redirect_to_login(self):
        unauthenticated_client = Client()
        response = unauthenticated_client.get(self.create_bookshelf_url)
        self.assertRedirects(response, '/accounts/login/?next=' + self.create_bookshelf_url)

    def test_redirects_to_bookshelf(self):
        response = self.client.get(self.create_bookshelf_url)
        self.assertRedirects(response, reverse('bookshelves'))

    def test_create_bookshelf(self):
        book_payload = {
            "book_data": "{'title': 'Imitação de Cristo', 'authors': ['Tomás de Kempis'], 'description': 'Da imitação de', 'ISBN_13': '', 'ISBN_10': '', 'categories': '', 'image_url': 'image.url'}"}

        self.client.post(self.create_bookshelf_url, data=book_payload)
        book_data = ast.literal_eval(book_payload['book_data'])
        book = Book.objects.filter(title=book_data['title'].lower())
        self.assertTrue(book.exists())


class TestEvaluateBookshelfView(BaseConfigTest):
    def get_evaluate_bookshelf_url(self, pk):
        return reverse('evaluate-bookshelf', args=[pk])

    def test_unauthenticated_client_redirect_to_login(self):
        bookshelf = self.sample_bookshelf(self.user)
        un_auth_client = Client()
        url = self.get_evaluate_bookshelf_url(bookshelf.pk)
        response = un_auth_client.get(url)
        self.assertRedirects(response, '/accounts/login/?next=' + url)

    def test_redirects_to_bookshelf(self):
        bookshelf = self.sample_bookshelf(self.user)
        url = self.get_evaluate_bookshelf_url(bookshelf.pk)
        response = self.client.get(url)
        self.assertRedirects(response, reverse('bookshelves'))

    def test_evaluate_bookshelf(self):
        bookshelf = self.sample_bookshelf(self.user)
        url = self.get_evaluate_bookshelf_url(bookshelf.pk)
        data = dict(evaluation=6)
        self.client.post(url, data=data)
        bookshelf.refresh_from_db()
        self.assertEqual(bookshelf.evaluation, data['evaluation'])

    def test_not_owner_evaluate_bookshelf(self):
        bookshelf = self.sample_bookshelf(self.other_user)
        url = self.get_evaluate_bookshelf_url(bookshelf.pk)
        data = dict(evaluation=5)
        self.client.post(url, data=data)
        bookshelf.refresh_from_db()
        self.assertEqual(bookshelf.evaluation, None, "other user must not be able to evaluate others bookshelf")
