from django.test import TestCase
from django.urls import reverse, resolve

from apps.bookshelves import views


class TestBookshelvesUrls(TestCase):
    def test_requested_bookshelves_url_resolve(self):
        url = reverse('bookshelves-requested')
        self.assertEqual(resolve(url).func.view_class, views.SharedBookShelvesRequestedListView)

    def test_shared_bookshelves_url_resolve(self):
        url = reverse('bookshelves-shared')
        self.assertEqual(resolve(url).func.view_class, views.SharedBookShelvesSharedListView)

    def test_returned_bookshelves_url_resolve(self):
        url = reverse('bookshelves-returned')
        self.assertEqual(resolve(url).func.view_class, views.SharedBookShelvesReturnedListView)

    def test_denied_bookshelves_url_resolve(self):
        url = reverse('bookshelves-denied')
        self.assertEqual(resolve(url).func.view_class, views.SharedBookShelvesDeniedListView)

    def test_user_bookshelves_url_resolve(self):
        url = reverse('bookshelves')
        self.assertEqual(resolve(url).func.view_class, views.BookShelvesListView)

    def test_create_bookshelf_url_resolve(self):
        url = reverse('create-bookshelf')
        self.assertEqual(resolve(url).func, views.create_bookshelf)

    def test_evaluation_url_resolve(self):
        url = reverse('evaluate-bookshelf', args=[1])
        self.assertEqual(resolve(url).func, views.evaluate_bookshelf)
