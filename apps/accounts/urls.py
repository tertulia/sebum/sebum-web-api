from django.urls import path

from . import views

urlpatterns = [
    path('register/', views.register, name='register'),
    path('login/', views.UserLoginView.as_view(), name='login'),
    path('logout/', views.UserLogoutView.as_view(), name='logout'),
    path('edit-profile/', views.EditProfileView.as_view(), name='edit-profile'),
    path('reset_password/', views.AccountPasswordResetView.as_view(), name="reset_password"),
    path('reset_password_complete/', views.AccountPasswordResetCompleteView.as_view(),
         name="password_reset_complete"),
    path('reset_password_sent/', views.AccountPasswordResetDoneView.as_view(), name="password_reset_done"),
    path('reset/<uidb64>/<token>/', views.AccountPasswordResetConfirmView.as_view(),
         name="password_reset_confirm"),
]
