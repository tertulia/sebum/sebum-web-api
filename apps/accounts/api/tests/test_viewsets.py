import random

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from apps.accounts.api.serializers import UserSerializer


def get_url(view_name, action=None, _id=None):
    """Return detail of specified url"""
    if _id:
        return reverse(f"api:{view_name}-{action}", args=[_id])
    return reverse(f"api:{view_name}-{action}")


def sample_user(**params):
    return get_user_model().objects.create_user(**params)


class TestEndpointUserAccount(TestCase):
    def setUp(self) -> None:
        self.user = sample_user(email='user@email.com', name='user name', password='passuser')
        self.client = APIClient()
        self.client.force_authenticate(self.user)

    def test_user_account_retrieve(self):
        serializer = UserSerializer(self.user, many=False)
        response = self.client.get(get_url('accounts', 'detail', self.user.id))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.keys(), serializer.fields.keys())

    def test_user_update_own_account(self):
        payload = {"name": 'new user name', 'telephone': "new @", "email": "new@email.com", 'bio': "new bio"}

        response = self.client.patch(get_url('accounts', 'detail', self.user.id), payload)
        self.user.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(self.user.name, payload['name'])
        self.assertEqual(self.user.telephone, payload['telephone'])
        self.assertEqual(self.user.email, payload['email'])
        self.assertEqual(self.user.bio, payload['bio'])

    def test_update_existing_telephone(self):
        other_user_telephone = '@other_user_telephone'
        sample_user(email='other_user@email.com', name='other name',
                    password='pass123', telephone=other_user_telephone)
        payload = {'telephone': other_user_telephone}
        response = self.client.patch(get_url('accounts', 'detail', self.user.id), payload)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['telephone'][0].code, 'unique')

    def test_user_update_other_account(self):
        payload = {"name": 'new user name', 'telephone': "new @", "email": "new@email.com"}
        other_user = sample_user(**payload)
        response = self.client.patch(get_url('accounts', 'detail', other_user.id), payload)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_retrieve_list_users(self):
        sample_user(email='user1@email', name='user 1', password='easy pass')
        sample_user(email='user2@email', name='user 2', password='easy pass')
        sample_user(email='user3@email', name='user 3', password='easy pass')

        response = self.client.get(get_url('accounts', 'list'))
        users = get_user_model().objects.all().exclude(id=self.user.id)

        serializer = UserSerializer(users, many=True)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)

    def test_retrieve_users_client_unauthorized(self):
        client = APIClient()
        response = client.get(get_url('accounts', 'list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_action_evaluate_user_success(self):
        other_user = sample_user(email='other@email.com', name='other user', password='some pass')

        evaluations = [random.randint(1, 10) for i in range(4)]
        mean = sum(evaluations) / len(evaluations)
        evaluation_url = get_url('accounts', 'evaluate', other_user.id)
        for evaluation in evaluations:
            response = self.client.post(evaluation_url, {"evaluation": evaluation}, format='json')
            self.assertEqual(response.status_code, status.HTTP_200_OK)

        other_user.refresh_from_db()
        self.assertAlmostEqual(other_user.evaluation, mean)

    def test_action_evaluate_user_invalid_payload(self):
        other_user = sample_user(email='other@email.com', name='other user', password='some pass')
        initial_evaluation = other_user.evaluation
        invalid_evaluation = 'must be a number'
        evaluation_url = get_url('accounts', 'evaluate', other_user.id)
        response = self.client.post(evaluation_url, {"invalid_evaluation": invalid_evaluation}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        other_user.refresh_from_db()
        self.assertEqual(other_user.evaluation, initial_evaluation)

    def test_expected_fields_in_payload(self):
        sample_user(email='user3@email', name='user 3', password='easy pass')

        response = self.client.get(get_url('accounts', 'list'))

        expected_fields = ["name", 'num_of_books', 'telephone', "email", 'evaluation', 'bio', 'image', 'last_login',
                           'created_at', 'city', 'state']
        user_data = response.data[0]  # first user in a list

        self.assertTrue(all([expected_field in user_data.keys() for expected_field in expected_fields]))

    def test_unexpected_fields_in_payload(self):
        sample_user(email='user3@email', name='user 3', password='easy pass')

        response = self.client.get(get_url('accounts', 'list'))

        unexpected_fields = ['cpf']

        user_data = response.data[0]  # first user in a list

        self.assertFalse(any([unexpected_field in user_data.keys() for unexpected_field in unexpected_fields]))


class TestEndpointAuthRegister(TestCase):
    REGISTER_USER_URL = reverse('rest_register')

    def setUp(self) -> None:
        self.cliente = APIClient()

    def test_register_success(self):
        data = {"email": "user@example.com", "password1": "strong_pass123", "password2": "strong_pass123",
                "name": "user name", "telephone": "some @", "bio": "a nice bio", "birth": "1992-09-17",
                "cpf": "1234567809", "city": "nice city", "state": "good state"}
        url = reverse('rest_register')
        response = self.cliente.post(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsNotNone(response.data['key'])

    def test_register_missing_fields(self):
        data = {}
        response = self.cliente.post(self.REGISTER_USER_URL, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['email'][0].code, 'required')
        self.assertEqual(response.data['name'][0].code, 'required')
        self.assertEqual(response.data['password1'][0].code, 'required')
        self.assertEqual(response.data['password2'][0].code, 'required')
        self.assertEqual(response.data['birth'][0].code, 'required')
        self.assertEqual(response.data['cpf'][0].code, 'required')
        self.assertEqual(response.data['city'][0].code, 'required')
        self.assertEqual(response.data['state'][0].code, 'required')

    def test_register_invalid_cpf(self):
        data = {"email": "user@example.com",
                "password1": "strong_pass123",
                "password2": "strong_pass123",
                "name": "user name",
                "telephone": "some @",
                "bio": "a nice bio",
                "birth": "1992-09-17",
                'invalid_cpf': '12345678912'
                }
        url = reverse('rest_register')
        response = self.cliente.post(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_register_existing_email(self):
        user_data = dict(email='user@email.com', password='this pass', name='user name')
        get_user_model().objects.create_user(**user_data)
        response = self.cliente.post(self.REGISTER_USER_URL, data=user_data)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['email'][0].code, 'invalid')


class TestEndpointAuthLogin(TestCase):
    LOGIN_URL = reverse('rest_login')

    def setUp(self) -> None:
        self.user_data = dict(email='user@email.com', password='passuser', name='user name')
        self.user = sample_user(**self.user_data)
        self.client = APIClient()

    def test_login_success(self):
        response = self.client.post(self.LOGIN_URL, data=self.user_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('key', response.data)

    def test_login_fail(self):
        data = dict(email="this@email.com", password='this pass')
        response = self.client.post(self.LOGIN_URL, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_login_no_email(self):
        data = dict(password='this pass')
        response = self.client.post(self.LOGIN_URL, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_login_no_password(self):
        data = dict(email='email@email.com')
        response = self.client.post(self.LOGIN_URL, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
