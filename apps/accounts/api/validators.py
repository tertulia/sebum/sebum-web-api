from rest_framework import exceptions


def validate_evaluation(data: dict):
    try:
        evaluation = float(data['evaluation'])

        if not (0 <= evaluation <= 10):
            msg = "Evaluation range is from 0 to 10"
            raise exceptions.ValidationError({"evaluation": msg})

    except KeyError:
        msg = "Field it is required"
        raise exceptions.ValidationError({"evaluation": msg})
    except TypeError:
        msg = "Field must be a number"
        raise exceptions.ValidationError({"evaluation": msg})
    return evaluation
