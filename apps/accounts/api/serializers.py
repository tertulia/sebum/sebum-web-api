from dj_rest_auth.registration.serializers import RegisterSerializer
from rest_framework import serializers

from . import validators
from ..models import User


class UserSerializer(serializers.ModelSerializer):
    """Serializers a my_user objetc"""

    num_of_books = serializers.SerializerMethodField()

    class Meta:
        model = User

        fields = ["name", 'num_of_books', 'telephone', "email", 'evaluation', 'bio', 'image', 'last_login',
                  'created_at', 'city', 'state']

        extra_kwargs = {'evaluation': {'read_only': True, },
                        'last_login': {'read_only': True, },
                        'created_at': {'read_only': True, }}

    def get_num_of_books(self, obj):
        return obj.bookshelf_set.count()

    def update_evaluation(self, data):
        evaluation = validators.validate_evaluation(data)
        self.instance.update_evaluation(evaluation)


class UserDetailSerializer(RegisterSerializer):
    name = serializers.CharField()
    cpf = serializers.CharField()
    birth = serializers.DateField()
    telephone = serializers.CharField()
    city = serializers.CharField()
    state = serializers.CharField()

    def get_cleaned_data(self):
        super(UserDetailSerializer, self).get_cleaned_data()
        return {
            "email": self.validated_data.get('email'),
            "password1": self.validated_data.get('password1'),
            "password2": self.validated_data.get('password2'),
            "name": self.validated_data.get('name'),
            "birth": self.validated_data.get('birth'),
            "cpf": self.validated_data.get('cpf'),
            "telephone": self.validated_data.get('telephone'),
            "city": self.validated_data.get('city'),
            "state": self.validated_data.get('state'),
        }
