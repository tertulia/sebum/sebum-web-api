from rest_framework import filters, mixins
from rest_framework import viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from . import permissions, serializers
from ..models import User


class UserViewSet(viewsets.GenericViewSet, mixins.RetrieveModelMixin, mixins.ListModelMixin, mixins.UpdateModelMixin):
    serializer_class = serializers.UserSerializer
    queryset = User.objects.all()
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated, permissions.UpdateOwnUser)
    filter_backends = (filters.SearchFilter,)
    search_fields = ("name", 'email',)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset().exclude(id=self.request.user.id))

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(methods=["post", ], detail=True, permission_classes=[IsAuthenticated, permissions.OnlyNotOwnUser])
    def evaluate(self, request, *args, **kwargs):
        serializer = self.serializer_class(self.get_object(), many=False)
        serializer.update_evaluation(request.data)
        return self.retrieve(request, *args, **kwargs)
