# Generated by Django 3.1.5 on 2021-08-20 19:10

import apps.services.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_remove_user_birth'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='cpf',
            field=models.CharField(default='12345678909', max_length=11, validators=[apps.services.validators.CPFValidator()]),
            preserve_default=False,
        ),
    ]
