from django.test import SimpleTestCase
from django.urls import reverse, resolve

from .. import views


class TestUrls(SimpleTestCase):
    def test_register_url_resolve(self):
        url = reverse('register')
        self.assertEquals(resolve(url).func, views.register)

    def test_login_url_resolve(self):
        url = reverse('login')
        self.assertEquals(resolve(url).func.view_class, views.UserLoginView)

    def test_logout_url_resolve(self):
        url = reverse('logout')
        self.assertEqual(resolve(url).func.view_class, views.UserLogoutView)

    def test_dashboard_url_resolve(self):
        url = reverse('edit-profile')
        self.assertEquals(resolve(url).func.view_class, views.EditProfileView)
