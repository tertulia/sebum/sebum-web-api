from django.contrib import auth
from django.contrib.auth import get_user_model
from django.test import TestCase, Client
from django.urls import reverse
from rest_framework import status


class BaseViewTestCase(TestCase):

    def sample_user(self, user_data=None):
        if user_data:
            return get_user_model().objects.create_user(**user_data)
        else:
            return get_user_model().objects.create_user(**self.user_data)

    def setUp(self) -> None:
        self.client = Client()
        self.user_data = dict(email='user@email.com',
                              name='user name',
                              password="some pass")


class TestRegisterViews(BaseViewTestCase):
    url = reverse('register')

    def setUp(self) -> None:
        super(TestRegisterViews, self).setUp()
        self.user_data['password2'] = self.user_data['password']

    def test_success_register_user(self):
        self.client.post(self.url, self.user_data, format='text/html')
        user = auth.get_user(self.client)

        self.assertTrue(get_user_model().objects.filter(email=self.user_data['email']).exists())
        self.assertTrue(user.is_authenticated)

    def test_success_register_user_redirect_to_home(self):
        response = self.client.post(self.url, self.user_data, format='text/html')
        self.assertRedirects(response, '/home')

    def test_register_user_with_used_email(self):
        self.client.post(self.url, self.user_data, format='text/html')

        new_client = Client()
        response = new_client.post(self.url, self.user_data, format='text/html')
        new_user = auth.get_user(new_client)

        self.assertFalse(new_user.is_authenticated)
        self.assertTemplateUsed(response, 'accounts/register.html')
        self.assertEqual(response.status_code, status.HTTP_409_CONFLICT)

    def test_register_user_unmatched_passwords(self):
        self.user_data['password2'] = 'diff from password'
        response = self.client.post(self.url, self.user_data, format='text/html')
        user = auth.get_user(self.client)

        self.assertFalse(get_user_model().objects.filter(email=self.user_data['email']).exists())
        self.assertFalse(user.is_authenticated)
        self.assertTemplateUsed(response, 'accounts/register.html')

    def test_register_user_others_methods(self):
        response = self.client.get(self.url)
        user = auth.get_user(self.client)
        self.assertFalse(user.is_authenticated)
        self.assertTemplateUsed(response, 'accounts/register.html')


class TestLoginView(BaseViewTestCase):
    url = reverse('login')

    def setUp(self) -> None:
        super(TestLoginView, self).setUp()
        self.sample_user()

    def test_success_login_user(self):
        self.client.post(self.url, {'username': self.user_data['email'],
                                    'password': self.user_data['password']},
                         format='text/html')
        user = auth.get_user(self.client)
        self.assertTrue(user.is_authenticated)

    def test_success_login_user_redirect_to_home(self):
        response = self.client.post(self.url, {'username': self.user_data['email'],
                                               'password': self.user_data['password']},
                                    format='text/html')
        self.assertRedirects(response, '/home')

    def test_fail_login_wrong_pass(self):
        self.user_data['password'] = 'wrong pass'
        self.client.post(self.url, {'username': self.user_data['email'],
                                    'password': self.user_data['password']},
                         format='text/html')
        user = auth.get_user(self.client)

        self.assertFalse(user.is_authenticated)

    def test_login_user_others_methods(self):
        response = self.client.get(self.url)
        user = auth.get_user(self.client)
        self.assertFalse(user.is_authenticated)
        self.assertTemplateUsed(response, 'accounts/login.html')


class TestLogoutView(BaseViewTestCase):
    url = reverse('logout')

    def test_logout_success(self):
        user = self.sample_user()
        self.client.force_login(user)

        self.client.post(self.url)
        user = auth.get_user(self.client)
        self.assertFalse(user.is_authenticated)

    def test_logout_redirect(self):
        response = self.client.post(self.url)
        self.assertRedirects(response, '/')


class TestEditProfileView(BaseViewTestCase):
    url = reverse('edit-profile')

    def setUp(self) -> None:
        super(TestEditProfileView, self).setUp()
        self.user = self.sample_user()
        self.client.force_login(self.user)

    def test_unauthenticated_user_access_edit_profile_redirects_to_login(self):
        client = Client()
        response = client.get(self.url)
        user = auth.get_user(client)

        self.assertFalse(user.is_authenticated)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertRedirects(response, '/accounts/login/?next=' + self.url)

    def test_edit_profile_template_used(self):
        response = self.client.get(self.url)
        self.assertTemplateUsed(response, 'accounts/edit_profile.html')
        self.assertTemplateUsed(response, 'base.html')
