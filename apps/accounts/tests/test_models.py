import random

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.utils import timezone as tz


def mean(evaluations: list) -> float:
    return (sum(evaluations)) / len(evaluations)


class TestModelUser(TestCase):
    def setUp(self) -> None:
        self.user = get_user_model().objects.create_user(
            email='user@email.com',
            name='complete user name',
            password='goodpass123',
        )

    def test_create_user_successfully(self):
        """Test creating a new user is successfully"""
        email = "test@email.com"
        name = 'name of user'
        password = "thispassword123"

        user = get_user_model().objects.create_user(
            email=email,
            name=name,
            password=password
        )

        self.assertEqual(user.email, email,
                         'email of user must be the same')
        self.assertTrue(user.check_password(password),
                        'password must be the same')

    def test_new_user_email_normalized(self):
        """Test the email for a new user is normalized"""
        email = "test@EMAIL.COM"
        user = get_user_model().objects.create_user(email, 'test123')

        self.assertEqual(user.email, email.lower(), 'email must be normalized')

    def test_new_user_invalid_email(self):
        """Test creating user with no email raiser error"""
        with self.assertRaises(ValueError):
            get_user_model().objects.create_user(None, 'test123')

    def test_new_user_invalid_name(self):
        """Test creating user with no email raiser error"""
        with self.assertRaises(ValueError):
            get_user_model().objects.create_user('new_user@email.com', None, 'test123')

    def test_create_new_superuser(self):
        """Test creating a new superuser"""
        user = get_user_model().objects.create_superuser(
            'test@email.com',
            'superuser name',
            'test123'
        )
        self.assertTrue(user.is_superuser, 'it must be a superuser')
        self.assertTrue(user.is_staff, 'it must be a staff')

    def test_string_representation(self):
        self.assertEqual(str(self.user), self.user.email)

    def test_get_full_name(self):
        self.assertEqual(self.user.get_full_name(), self.user.name)

    def test_get_short_name(self):
        self.assertEqual(self.user.get_short_name(), 'complete')

    def test_update_evaluation(self):
        num_of_evaluation = self.user.num_of_evaluations
        evaluations = [random.randint(1, 10) for i in range(5)]

        for evaluation in evaluations:
            self.user.update_evaluation(evaluation)

        self.assertAlmostEqual(mean(evaluations), self.user.evaluation)
        self.assertEqual(self.user.num_of_evaluations, num_of_evaluation + len(evaluations))

    def test_update_last_login(self):
        self.user.last_login = tz.now() - tz.timedelta(days=-10)
        self.user.save()
        self.user.update_last_login()
        self.assertAlmostEqual(self.user.last_login, tz.now(), delta=tz.timedelta(seconds=20))
