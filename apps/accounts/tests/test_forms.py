from django.contrib.auth import get_user_model
from django.test import TestCase, Client
from django.urls import reverse

from ..forms import EditUserProfileForm


class UnitTestAccountForm(TestCase):
    def setUp(self) -> None:
        self.user_data = dict(
            email='user@email.com',
            name='user name',
            telegram="some name",
            bio="a nice bio about the user",
        )

    def test_user_form_valid_data(self):
        form = EditUserProfileForm(data=self.user_data)
        self.assertTrue(form.is_valid())

    def test_user_form_no_data(self):
        form = EditUserProfileForm(data={})
        self.assertFalse(form.is_valid())

    def test_user_invalid_email(self):
        self.user_data['email'] = 'not an email'
        form = EditUserProfileForm(data=self.user_data)
        self.assertFalse(form.is_valid())


class IntegrationTestAccountForm(TestCase):
    def setUp(self) -> None:
        self.user_data = dict(
            email='user@email.com',
            name='user name',
            telephone="99999999999",
            bio="a nice bio about the user",
        )
        self.user = get_user_model().objects.create_user(**self.user_data)
        self.client = Client()
        self.client.force_login(self.user)
        self.url = reverse('edit-profile')

    def test_invalid_email(self):
        response = self.client.post(self.url, data={"email": "bad email"})
        self.assertContains(response, 'Insira um endereço de email válido.')
