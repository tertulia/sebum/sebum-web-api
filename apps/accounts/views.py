from django.contrib import messages, auth
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.views import (LoginView, LogoutView, PasswordResetCompleteView, PasswordResetView,
                                       PasswordResetDoneView, PasswordResetConfirmView)
from django.shortcuts import render, redirect
from django.views.generic import UpdateView
from rest_framework import status

from apps.accounts.forms import EditUserProfileForm


class UserAnonymousTestMixin(UserPassesTestMixin):
    def test_func(self):
        return self.request.user.is_anonymous

    def handle_no_permission(self):
        return redirect('logout')


def register(request):
    if request.user.is_authenticated:
        return redirect('index')

    elif request.method == 'POST':

        name = request.POST['name']
        email = request.POST['email']
        password = request.POST['password']
        password2 = request.POST['password2']

        # Check if passwords match
        if password == password2:
            # Check username
            if get_user_model().objects.filter(email=email).exists():
                messages.warning(request, 'Email já cadastrado')
                return render(request, 'accounts/register.html', status=status.HTTP_409_CONFLICT)

            else:
                # Looks good
                user = get_user_model().objects.create_user(name=name, password=password, email=email)

                auth.login(request, user)
                messages.success(request, 'You are now logged in')
                return redirect('home')

        else:
            messages.warning(request, 'As senhas não coincidem')
            return render(request, 'accounts/register.html', status=status.HTTP_400_BAD_REQUEST)
    else:
        return render(request, 'accounts/register.html')


class UserLoginView(LoginView):
    template_name = 'accounts/login.html'
    redirect_authenticated_user = True


class UserLogoutView(LogoutView):
    next_page = 'index'


class EditProfileView(LoginRequiredMixin, UpdateView):
    model = get_user_model()
    template_name = 'accounts/edit_profile.html'
    form_class = EditUserProfileForm

    def get_object(self, queryset=None):
        return self.request.user


class AccountPasswordResetCompleteView(UserAnonymousTestMixin, PasswordResetCompleteView):
    template_name = 'accounts/password_reset_complete.html'


class AccountPasswordResetView(UserAnonymousTestMixin, PasswordResetView):
    template_name = 'accounts/password_reset_form.html'
    email_template_name = 'accounts/password_reset_email.html'


class AccountPasswordResetDoneView(UserAnonymousTestMixin, PasswordResetDoneView):
    template_name = 'accounts/password_reset_done.html'


class AccountPasswordResetConfirmView(UserAnonymousTestMixin, PasswordResetConfirmView):
    template_name = 'accounts/password_reset_confirm.html'
