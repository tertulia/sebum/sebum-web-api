from django import forms
from django.contrib.auth import get_user_model


class EditUserProfileForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ["name", "telephone", "image", "bio", 'email']
        labels = {"name": "Nome",
                  "image": "Imagem",
                  }
        widgets = dict(
            image=forms.FileInput(attrs={"type": "submit",
                                         "class": "btn btn-fill btn-primary",
                                         "value": "Nova image"}),
            name=forms.TextInput(attrs={"class": "form-control"}),
            telephone=forms.TextInput(attrs={"class": "form-control"}),
            bio=forms.Textarea(attrs={"class": "form-control"}),
            email=forms.TextInput(attrs={"class": "form-control"})
        )
