from rest_framework import routers

from apps.accounts.api.viewsets import UserViewSet
from apps.bookclubs.api.viewsets import BookClubViewSet
from apps.books.api.viewsets import BookViewSet
from apps.bookshelves.api.viewsets import BookShelvesViewSet, BookShelfViewSet, SharedBookShelfViewSet
from apps.bookssimilarities.api.viewsets import BooksSimilaritiesViewSet

router = routers.DefaultRouter()

router.register(r"accounts", UserViewSet, basename="accounts")
router.register(r'books', BookViewSet, basename='books')
router.register(r'bookshelves', BookShelvesViewSet, basename='bookshelves')
router.register(r'bookshelf', BookShelfViewSet, basename='bookshelf')
router.register(r'shared-bookshelf', SharedBookShelfViewSet, basename='shared_bookshelf')
router.register(r'recommendations', BooksSimilaritiesViewSet, basename='recommendations')
router.register(r'bookclubs', BookClubViewSet, basename='bookclubs')
