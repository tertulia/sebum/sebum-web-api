## Resumo

(Resuma o que esse módulo visa fazer)


## Qual é o propóstio desse módulo

(Faça uma boa descrição sobre o propósito desse módulo)

### Descrição da estória de usuário

- **COMO UM(A)** ( tipo de usuário ) **EU QUERO** ( realizer alguma tarefa ) **PARA QUE EU POSSA** ( alcançar um objetivo ).


### Critério de aceitação

- **DADO* ( algum contexto ) **QUANDO** ( alguma  açao que é realizada ) **ENTÃO** (um conjunto de resultados observáveis deve ocorrer).

/label ~new_module ~suggestion
/due < in 2 days | this friday | december 31st >
/weight < 1 2 3 >
/assign @





## Problema Relacionado

#<número do problema>

## O que deve ser melhorado?

(Qual recurso deve ser melhorado)


## Soluções possíveis

(Liste as possíveis soluções de refatoração)

### Descriçao da estória de usuário

- **COMO UM(A)** ( tipo de usuário ) **EU QUERO** ( realizer alguma tarefa ) **PARA QUE EU POSSA** ( alcançar um objetivo ).

### Criterio de aceitaçao 

- **DADO* ( algum contexto ) **QUANDO** ( alguma  açao que é realizada ) **ENTÃO** (um conjunto de resultados observáveis deve ocorrer).


/label ~refactor
/due < in 2 days | this friday | december 31st >
/weight < 1 2 3 >
/assign @
