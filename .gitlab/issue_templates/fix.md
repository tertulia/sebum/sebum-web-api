## Resumo

(Resuma o bug encontrado de forma concisa)

## Passos para reproduzir

(Como se pode reproduzir o problema - isso é muito importante)


## Qual é o comportamento do bug atual?

(O que realmente acontece)


## Qual é o comportamento correto esperado?

(O que você deveria ver ao invés)


## Registros e / ou capturas de tela relevantes

(Cole todos os registros relevantes - por favor, use blocos de código (```) para formatar a saída do console,
logs e código, pois de outra forma é muito difícil de ler.)


## Possíveis correções

(Se puder, link para a linha de código que pode ser responsável pelo problema)

/label ~fix ~needs-investigation
/due < in 2 days | this friday | december 31st >
/weight < 1 2 3 >
/assign @
