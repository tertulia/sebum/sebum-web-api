## O que deve ser testado

## Issue Relacionada

(Inclua o problema / recurso relacionado ao teste)

# <número do problema>


## Qual é o comportamento atual?

(Exceção que não está sendo testada)

/label ~test
/due < in 2 days | this friday | december 31st >
/weight < 1 2 3 >
/assign @
