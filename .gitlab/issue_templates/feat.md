## Resumo

(Resuma de forma concisa o que deve ser feito)


## Estória de usuário

### Descriçao da estória de usuário

- **COMO UM(A)** ( tipo de usuário ) **EU QUERO** ( realizer alguma tarefa ) **PARA QUE EU POSSA** ( alcançar um objetivo ).

### Criterio de aceitaçao 

- **DADO* ( algum contexto ) **QUANDO** ( alguma  açao que é realizada ) **ENTÃO** (um conjunto de resultados observáveis deve ocorrer).

## Mockup

(Wiki link to mockup ou imagem)


/label ~feat
/due < in 2 days | this friday | december 31st >
/weight < 1 2 3 >
/assign @
